jQuery(document).ready(function($) {
    ajax_url = the_ajax_script_app.ajaxurl;




    /* center modal */
    function centerModals() {
        $('.modal').each(function (i) {
            var $clone = $(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            $(this).find('.modal-content').css("margin-top", top);
        });
        //var banner_height = $('.owl-wrapper').height();
        //$('.banner-tag').css('height',banner_height+"px");
        //var banner_page_height = $('.bannerimg').height();
        //$('.banner-tag').css('height',banner_page_height+"px");
        var banner_height = $('.bx-wrapper').height();
        $('.banner-tag').css('height',banner_height+"px");
        var banner_page_height = $('.bannerimg').height();
        $('.banner-tag').css('height',banner_page_height+"px");
    }

    $('.modal').on('show.bs.modal', centerModals);
    $(window).on('resize', centerModals);

    centerModals();

    scrollNav = false;
    function updateNav(scrollNav){
        var posScroll = $(document).scrollTop();
        //var header_pos = $('#header .bg-white').height();

        if(scrollNav) {
            //console.log(posScroll);

            if (posScroll >= 39) {
                //$('.mini-logo').show('fast');
                //$('#header').addClass('mini');
                //$('#header').next().addClass('mini-jump');
                //$('.header-logo').addClass('make-small-logo');

                //$('.mini-jump').css('margin-top',header_pos+'px');
                //$('#mobmenu').addClass('mini');
                //$('#mobmenu').css('top',header_pos+'px');

                if (posScroll > 40) {
                    $('.bannerimg').css('top',(+(posScroll*0.25)-7)+'px');
                    //$('.homepage-slider').css('top',(+(posScroll*0.75)-7)+'px');
                }
            }
            else {
                //$('.mini-logo').hide('fast');
                //$('#header').removeClass('mini');
                //$('#mobmenu').removeClass('mini');
                //$('.mini-jump').css('margin-top','0px');
                //$('#header').next().removeClass('mini-jump');
                //$('.header-logo').removeClass('make-small-logo');
                //$('#mobmenu').css('top','inherit');
                $('.bannerimg').css('top','inherit');
                //$('.homepage-slider').css('top','inherit');
            }
        }
        else{
            if(posScroll <39) {
                //$('.mini-logo').show('fast');
                //$('#header').addClass('mini');
                //$('#mobmenu').addClass('mini');
                //$('#header').next().addClass('mini-jump');
                //$('.header-logo').addClass('make-small-logo');
                $('.bannerimg').css('top','inherit');
                //$('.homepage-slider').css('top','inherit');

                //$('#mobmenu').css('top',header_pos+'px');
                //$('.mini-jump').css('margin-top',header_pos+'px');
            }
        }
    }
    $(window).on('load resize', function(e){
        var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if(width >= 768){
            scrollNav = true;
        } else {
            scrollNav = false;
        }
    });
    $(window).on('load resize scroll', function(e){
        updateNav(scrollNav);
    });

    // menu
    $(window).scroll(function(){
        if ($(this).scrollTop() > 35) {
            $('#scroll-menu').fadeIn(500);
        } else {
            $('#scroll-menu').fadeOut(500);
        }
    });

    // hide #back-top first
    jQuery("#back-top").hide();
    // fade in #back-top
    jQuery(function () {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 300) {
                jQuery('#back-top').fadeIn();
            } else {
                jQuery('#back-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        jQuery('#back-top a').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 400);
            return false;
        });
        jQuery('.anchormate').click(function () {
            jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top
            }, 500);
            return false;
        });
    });
});