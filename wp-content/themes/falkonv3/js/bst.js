(function ($) {

	"use strict";

	$(document).ready(function() {

		// Comments

		$(".commentlist li").addClass("panel panel-default");
		$(".comment-reply-link").addClass("btn btn-default");

		// Forms

		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		// You can put your own code in here



        //$('.maincta-links').click(function(){
        //    var collapsed=$(this).find('i').hasClass('fa fa-angle-down');
        //       $('.maincta-links').find('i').removeClass('fa-angle-up');
        //       $('.maincta-links').find('i').addClass('fa fa-angle-down');
        //    if(collapsed)
        //        $(this).find('i').toggleClass('fa-angle-down fa-2x fa-angle-up fa-2x')
        //});

        // Modal / sticky Toggle
        jQuery('.navbar-toggle').on('click', function() {

            $(this).parents("nav").toggleClass("scroller_default scroller_clear");

            if (jQuery(this).hasClass('collapsed')) {
                $(this).removeClass('collapsed');
            }
            else
            {
                $(this).addClass('collapsed');
            }
        });

        // Video Button
        $('.launch-modal').on('click', function(e) {
            $('.hme-nav-wrapper').addClass("no-vis");
            e.preventDefault();
        });
        $('.close-video').on('click', function(e) {
            $('.hme-nav-wrapper').removeClass("no-vis");
            e.preventDefault();
        });


        // The .sublicker will open .dropdown-menu when clicked
        $('.subclicker').click(function(e){
            //e.preventDefault();
            //$(".sub-menu", this).slideToggle("fast");
            $(this).next('.dropdown-menu').toggleClass('sub-active');
            //$(this).parent('.menu-item').toggleClass('active');
            $(this).toggleClass('fa-angle-down fa-angle-up');
        });

        // hide caret .subclicker on dropdown
        $( ".dropdown-menu .subclicker" ).addClass( "hide-subcli" );


	});


}(jQuery));
