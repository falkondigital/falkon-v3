<?php
// Falkon Options
global $falkon_option;
//var_dump($falkon_option);
?>

<?php if (!is_page($falkon_option['falkon_company_contactus_link'])) {
    ?>
    <section id="pre-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                    <?php echo $falkon_option['falkon_company_prefooter_txt'] ?>
                    <div class="visible-xs visible-sm"><br></div>
                    <a href="<?php echo get_permalink($falkon_option['falkon_company_contactus_link']); ?>" class="btn ghost-btn" role="button">
                        Get In Touch
                    </a>
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center footer-socials-wrapper">
                <?php
                //Social links
                $block_location = 'footer-social-links';
                $social_links_block = falkon_get_transient($block_location);
//                _log($social_links_block);
                echo $social_links_block;
                ?>
            </div>
            <?php
//                echo '<br>';
//                   $thumb_array = $falkon_option['falkon_company_footer_'];
//
//                   foreach($thumb_array as $image_id) {
//                       $image_attributes = wp_get_attachment_image_src( $image_id, 'full-size');
//                       echo '<img src="'.$image_attributes[0].'" class="cert-imgs">';
//                   }

                if(is_array($falkon_option['falkon_footer_accreditation_logos'])){
                    $result = '<div class="col-xs-12 footer-accreditation-logos text-center">';
                    foreach ($falkon_option['falkon_footer_accreditation_logos'] as $image_id) {
                        $image_attributes = wp_get_attachment_image_src($image_id, 'full');
                        $img_meta       = wp_prepare_attachment_for_js($image_id);
                        $image_title    = $img_meta['title'] == '' ? '' : $img_meta['title'];
                        $image_alt      = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];

                        $retina_image = '';
                        $info = get_attached_file($image_id);
                        $source_dirname  = pathinfo($info, PATHINFO_DIRNAME);
                        $source_filename = pathinfo($info, PATHINFO_FILENAME);
                        $source_ext      = pathinfo($info, PATHINFO_EXTENSION);

                        $file_explode = explode('.',$info);
                        if (file_exists(trailingslashit($source_dirname).$source_filename.'@2x.'.$source_ext))
//                            if (file_exists($file_explode[0].'@2x.'.$file_explode[1]))
                            $retina_image = " data-retina";
                        $result .= '<img src="' . $image_attributes[0] . '" title="'.esc_attr($image_title).'" alt="'.esc_attr($image_alt).'" '.$retina_image.'>';
                    }
                    $result .= '</div>';
                    echo $result;
                }
            ?>
            <div class="col-xs-12 footer-blurb text-center">
            <?php
            $menu_name = 'footer-nav';
            $menu_args = array(
                'theme_location'    => $menu_name,
                'menu' => 'main-nav',
                'depth'             => 3,
                'menu_class'        => 'footer-links-nav',
                'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                'walker'            => new wp_bootstrap_navwalker()
            );
            echo yourtheme_nav_menu_args($menu_name,$menu_args);

                $footer_blurb = $falkon_option['falkon_footer_blurb']? str_replace('{{year}}',date('Y'),$falkon_option['falkon_footer_blurb']):'&copy; '.date('Y').' <a href="'.home_url( '/' ).'" title="'. esc_attr( get_bloginfo( 'name', 'display' ).' - '.get_bloginfo('description', 'display') ).'" rel="home nofollow">'.get_bloginfo( 'name' ).'</a>';

                echo wpautop($footer_blurb.'   |   Registered in the UK. Company no '.$falkon_option['falkon_company_register_no'].' <span class="text-nowrap">VAT No: '.$falkon_option['falkon_company_vat_no'].'</span>.');
//                echo '<br>';
//                echo str_replace("<br />",", ",$falkon_option['falkon_registered_address']).'  |  <a href="tel:'.$falkon_option['falkon_company_phone_no'].'" class="telephone">'.$falkon_option['falkon_company_phone_no'].'</a>   |   <a href="'.get_permalink($falkon_option['falkon_company_termspage_link']).'">Terms & Conditions</a>';

//                $footlinks = wp_nav_menu( array(
//                    'container' => false,
//                    'theme_location' => 'footlinkmenu',
//                    'fallback_cb' => false,
//                    'before'		=> ' | ',
//                    'echo'			=> 0,
//                    'menu_id'		=> 'footlinkmenu',
//                    'items_wrap'		=> '<ul id="%1$s" class="menu-inline-links %2$s">%3$s</ul>') );
                $menu_name = 'footlinkmenu';
                $menu_args = array(
                    'theme_location'    => $menu_name,
                    'container' => false,
                    'fallback_cb' => false,
                    'before'		=> ' | ',
                    'echo'			=> false,
                    'menu_id'		=> 'footlinkmenu',
                    'items_wrap'		=> '<ul id="%1$s" class="menu-inline-links %2$s">%3$s</ul>'
                );
                $footlinks = yourtheme_nav_menu_args($menu_name,$menu_args);
                ?>

                <?php
                $footer_email = '';//$falkon_option['falkon_site_email']? ' | <a href="mailto:'.$falkon_option['falkon_site_email'].'" title="'.esc_attr($falkon_option['falkon_site_email']).'" rel="nofollow" class="footeremail">'.$falkon_option['falkon_site_email'].'</a>': '';
                echo '<span class="footer-class footerlinks"><span class="address">'.str_replace("<br />",", ",$falkon_option['falkon_registered_address']).'</span> |  <a href="tel:'.$falkon_option['falkon_company_phone_no'].'" class="telephone">'.$falkon_option['falkon_company_phone_no'].'</a>'.$footlinks.'</span>';
                ?>
                <?php //$footer_blurb = $falkon_option['falkon_footer_blurb']? str_replace('{{year}}',date('Y'),$falkon_option['falkon_footer_blurb']):'&copy; '.date('Y').' <a href="'.home_url( '/' ).'" title="'. esc_attr( get_bloginfo( 'name', 'display' ).' - '.get_bloginfo('description', 'display') ).'" rel="home nofollow">'.get_bloginfo( 'name' ).'</a>';
                //echo wpautop($footer_blurb);
                ?>
            </div>
        </div>
    </div>
</footer>
<?php /* ?><script src="https://maps.googleapis.com/maps/api/js"></script><?php */ ?>
<script>
    // Owl
    jQuery(document).ready(function ($) {
//        $("#falkon-client-carousel").owlCarousel({
////			autoPlay: 3000, //Set AutoPlay to 3 seconds
//            items : 4,
//            itemsDesktop : [1199,4],
//            itemsDesktopSmall : [979,3],
//            pagination : false,
//            navigation:true,
//            navigationText: [
//                "<i class='fa fa-chevron-left fa-3x'></i>",
//                "<i class='fa fa-chevron-right fa-3x'></i>"
//            ]
//        });

//        $("#falkon-testimonials").owlCarousel({
//            singleItem : true,
//            pagination:false,
//            transitionStyle : "fade",
//            autoPlay: $("#falkon-testimonials > div").length > 1 ? 5000 : false
//        });


    });
</script>
<?php wp_footer(); ?>
<?php
if(FALKON_SHOW_SITE_TIMER){
    global $global_timer;
    echo '<div class="w-25 p-1 text-white bg-primary fixed-bottom " id="site-timers" >';
    echo '<p><small>TRANSIENTS = '. (int)FALKON_USE_TRANSIENTS.', ';
    echo 'TIMERS = '. (int)FALKON_SHOW_TIMERS.', ';
    echo 'SITE_TIMER = '. (int)FALKON_SHOW_SITE_TIMER.'</small></p>';
    echo '<p class="font-weight-bold mb-0">'.number_format( microtime(true) - $global_timer, 5 ).' SECONDS</p>';//TESTING
    echo '</div>';
}
?>
<p id="back-top">
    <a href="#top" class=" " rel="nofollow"><i class="fa fa-chevron-up"></i></a>
</p>
</body>
</html>
