<?php
/*Template Name: Apply Online */
get_header();

get_template_part( 'includes/blocks/subpg', 'list' );

echo '<div id="contact-us" class="container">';

echo '<div class="col-xs-12 col-sm-12 text-center contact-cnt">';
    get_template_part('includes/loops/content-custom-title');
echo '</div>';
?>

  <div id="apply-cf7">
      <?php echo do_shortcode("[contact-form-7 id='953' title='Apply Online']");?>
  </div>



<?php
echo '</div>';
get_footer();

?>