<?php
/*Template Name: Our People Page */
get_header();

get_template_part( 'includes/blocks/subpg', 'list' );

echo '<div id="our-people" class="container">';

$args = array(
    'paged'             =>  false,
    'post_type'         =>  'staff_member',
    'post_status'	    =>  'publish',
//		'orderby'		    =>  'menu_id date',
//		'order'             =>  'ASC',
    'posts_per_page' => -1

);
$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
    while ( $the_query->have_posts() ) :
        $the_query->the_post();

        global $ourpeople_meta;
        $people_align = $ourpeople_meta->the_meta();

        echo '<div class="hidden-xs visible-sm visible-md visible-lg">';


          // Right image alignment
          if (isset( $people_align['people_alignment'])) {
              $result = '<div class="col-xs-12 col-sm-12 peep-pad">';
              $result .= '<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">';
                $result .=  '<h2>'.get_the_title().'</h2>';
                $result .= wpautop ( get_the_content(),true);
              $result .= '</div>';
              $result .= '<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">';
                 $result .= get_the_post_thumbnail();
              $result .= '</div>';
              $result .= '</div>';

          echo $result;
          }

          // Left image alignment
          else {
              $result = '<div class="col-xs-12 col-sm-12 peep-pad">';
              $result .= '<div class="col-xs-12 col-sm-5 col-md-4 col-lg-3">';
                 $result .= get_the_post_thumbnail();
              $result .= '</div>';
              $result .= '<div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">';
                $result .=  '<h2>'.get_the_title().'</h2>';
                $result .= wpautop ( get_the_content(),true);
              $result .= '</div>';
              $result .= '</div>';

          echo $result;
          }

        echo '</div>';


        // Mobile all full width center
        echo '<div class="visible-xs hidden-sm hidden-md hidden-lg">';
            $result = '<div class="col-xs-12 col-sm-12 peep-pad text-center">';
            $result .= '<div class="col-xs-12 col-sm-12">';
                 $result .= get_the_post_thumbnail();
            $result .= '</div>';
            $result .= '<div class="col-xs-12 col-sm-12">';
                  $result .=  '<h2>'.get_the_title().'</h2>';
                  $result .= wpautop ( get_the_content(),true);
            $result .= '</div>';
            $result .= '</div>';

        echo $result;

        echo '</div>';





    endwhile;
    wp_reset_postdata();

}

echo '</div>';
get_footer();

?>