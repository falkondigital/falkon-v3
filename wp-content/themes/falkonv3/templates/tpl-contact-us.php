<?php
/*Template Name: Contact us */
?>
<?php global $falkon_option;?>
<?php get_header(); ?>
    <section id="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                        <div class="h1-pt text-center">How to say hello</div>
                        <hr class="pink">
                        <p class="text-center">We're easy to find and you can contact us via email, the contact form below or even the good old fashioned way on the phone.</p>
                    <div id="map-canvas">
                        <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19039.470033903028!2d-2.369203700372436!3d53.38023477083114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487bac97be94865b%3A0x71af4b7e692ffd38!2zRmFsa29uIERpZ2l0YWwgLSBZb3VyIERpZ2l0YWwgUGFydG5lcnMg4pyU77iP8J-Srw!5e0!3m2!1sen!2suk!4v1533297299128
"
                            width="100%"
                            height="500"
                            frameborder="0"
                            style="border:0"
                            allowfullscreen>
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="grey-page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <p class="h1-pt text-center">Contact Information</p>
                    <hr class="pink">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 text-center">
                    <?php echo do_shortcode("[contact-form-7 id='9533' title='New Contact Form']"); ?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4">
                    <p class="h3">Contact Information</p>
                    <ul class="contactli">
                        <li class="location"><?php echo $falkon_option['falkon_registered_address'];?></li>
                        <li class="phone"><a href="tel:<?php echo $falkon_option['falkon_company_phone_no'];?>"><?php echo $falkon_option['falkon_company_phone_no'];?></a></li>
                        <li class="email"><a href="mailto:<?php echo $falkon_option['falkon_contact_email'];?>"><?php echo $falkon_option['falkon_contact_email'];?></a></li>
                    </ul>
                   <?php /* ?> <div id="contacts-info-details" class="py-4">
                        <?php
                        $company_name = $falkon_option['falkon_company_name']!=''? $falkon_option['falkon_company_name']: get_bloginfo( 'name' );
                        //echo '<div itemprop="name">'.$company_name.'</div>';
                        ?>
                        <?php if($falkon_option['falkon_contact_address']!='') echo '<p><i class="fa fa-building text-pink" aria-hidden="true"></i> '.nl2br($falkon_option['falkon_contact_address']).'</p>';?>
                        <?php if($falkon_option['falkon_registered_address']!='') echo '<p><i class="fa fa-building text-pink" aria-hidden="true"></i> '.nl2br($falkon_option['falkon_registered_address']).'</p>';?>
                        <?php if($falkon_option['falkon_contact_number']!='') echo '<p><i class="fa fa-phone-square text-pink"></i> '.do_shortcode("[company-contact-number link=true]").'</p>';?>
                        <?php if($falkon_option['falkon_company_phone_no']!='') echo '<p><i class="fa fa-phone-square text-pink"></i> '.do_shortcode("[company-contact-number link=true]").'</p>';?>
                        <?php if($falkon_option['falkon_contact_fax']!='') echo '<p><i class="fa fa-fax text-pink" aria-hidden="true"></i> '.do_shortcode("[company-contact-fax link=true]").'</p>';?>
                        <?php if($falkon_option['falkon_contact_email']!='') echo '<p><i class="fa fa-envelope-square text-pink"></i> '.do_shortcode("[company-contact-email link=true]").'</p>';?>
                        <?php /* ?><p><?php echo ($falkon_option['falkon_company_name']!=''?$falkon_option['falkon_company_name']:bloginfo('name'));?></p><?php */ ?>
                        <?php //if($falkon_option['falkon_opening_hours']!='') echo '<p>Opening hours are '.do_shortcode("[company-opening-hours]").'</p>';?>
                    <?php /* ?></div><?php */ ?>
                </div>
            </div>
        </div>
    </section>
<div id="contact-content">
    <section id="page-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                    <?php get_template_part('includes/loops/content-contact'); ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>
