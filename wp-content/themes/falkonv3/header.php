<?php
global $falkon_option;
if(FALKON_SHOW_SITE_TIMER){
    global $global_timer;
    $global_timer = microtime(true);        //TESTING
}
?>
<!DOCTYPE html>
<html class="no-js">
<head>
	<title><?php wp_title('|', true, 'right'); ?></title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
  	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php   //Favicon
    $favicon_filename = get_template_directory().'/images/ico/favicon.ico';
    if (file_exists($favicon_filename)){
        $favicon_url = get_template_directory_uri().'/images/ico/favicon.ico';
        echo '<link rel="icon" href="'.$favicon_url.'">';
        echo '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-144-precomposed.png">';
        echo '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-114-precomposed.png">';
        echo '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-72-precomposed.png">';
        echo '<link rel="apple-touch-icon-precomposed" href="'.get_template_directory_uri().'/images/ico/apple-touch-icon-57-precomposed.png">'."\n";
    }
    ?>
    <?php
    if(isset($falkon_option['falkon_theme_colour'])){
    echo '<meta name="theme-color" content="'.(substr($falkon_option['falkon_theme_colour'], 0,1)!='#'?'#'.$falkon_option['falkon_theme_colour']:$falkon_option['falkon_theme_colour']).'" />';
    echo "\n";
    }
    ?>
    <?php //include(locate_template('includes/blocks/google-analytics.php'));?>
    <?php do_action('falkon_hook_before_wp_head');?>
    <link rel="manifest" href="/manifest.json">
    <script src="/index.js" async></script>
    <?php
    $company_name = $falkon_option['falkon_schema_business_name']!=''? $falkon_option['falkon_schema_business_name']: get_bloginfo( 'name' );
    $social_links_header = $falkon_option['falkon_multicheckbox_inputs'];
    $sameas = '';
    if(count($social_links_header)>0){
        $sameas = '"sameAs" : [';
        foreach($social_links_header as $social_url => $value){
            if($value and $falkon_option[$social_url]!='')
                $sameas .= '"'.esc_url($falkon_option[$social_url]).'",';
        }
        $sameas = substr($sameas,0,-1);
        $sameas .= ' ]';
    }
    $street_address = '"'.$falkon_option['falkon_schema_location_streetaddress1'].'"'.($falkon_option['falkon_schema_location_streetaddress2']?', "'.$falkon_option['falkon_schema_location_streetaddress2'].'"':'');
    ?>
    <script type="application/ld+json">
		{
		  "@context": {
			"@vocab": "http://schema.org/"
		  },
		  "@graph": [
			{
			  "@id": "<?php echo get_site_url();?>",
			  "@type": "Organization",
			  "name": "<?php echo $company_name;?>",
			  "url" : "<?php echo get_site_url();?>",
			  "logo" : "<?php echo get_stylesheet_directory_uri();?>/images/falkon-digital-logo.png",
			  <?php echo $sameas; ?>
			},
			{
			  "@type": "LocalBusiness",
			  "parentOrganization": {
				  "name" : "<?php echo $company_name;?>"
			  },
			 "name" : "<?php echo $company_name;?>",
			 "image" : "<?php echo get_stylesheet_directory_uri();?>/images/falkon-digital-logo.png",
			  "address": {
				  "@type" : "PostalAddress",
				  <?php if($falkon_option['falkon_schema_location_streetaddress1']) echo '"streetAddress": ['.$street_address.'],'; ?>
				  <?php if($falkon_option['falkon_schema_location_addressLocality']) echo '"addressLocality": "'.$falkon_option['falkon_schema_location_addressLocality'].'",'; ?>
				  <?php if($falkon_option['falkon_schema_location_addressRegion']) echo '"addressRegion": "'.$falkon_option['falkon_schema_location_addressRegion'].'",'; ?>
				  <?php if($falkon_option['falkon_schema_location_postalCode']) echo '"postalCode": "'.$falkon_option['falkon_schema_location_postalCode'].'",'; ?>
				  <?php if($falkon_option['falkon_company_phone_no']) echo '"telephone": "'.$falkon_option['falkon_company_phone_no'].'"'; ?>
				  },
			  <?php if($falkon_option['falkon_schema_opening_hours']) echo '"openingHours": [ "'.$falkon_option['falkon_schema_opening_hours'].'" ],'; ?>
			  <?php if($falkon_option['falkon_company_vat_no']) echo '"vatID": "'.$falkon_option['falkon_company_vat_no'].'",'; ?>
			  <?php if($falkon_option['falkon_schema_location_geo']) echo '"geo": "'.$falkon_option['falkon_schema_location_geo'].'",'; ?>
			  <?php if($falkon_option['falkon_schema_has_map']) echo '"hasmap": "'.$falkon_option['falkon_schema_has_map'].'"'; ?>

			}
		  ]
		}
	</script>
    <?php
    $schema = get_post_meta(get_the_ID(), 'schema', true);
    if($schema!='') {
        echo $schema;
    }
    ?>
    <?php
    $schema_video_object = get_post_meta(get_the_ID(), 'schema_video_object', true);
    if($schema_video_object!='') {
        foreach($schema_video_object as $schema_video_array){
            echo '<script type=application/ld+json>{
                        "@context": "http://schema.org",
                        "@type": "VideoObject",
                        "name": "'.$schema_video_array['title'].'",
                        "description": '.json_encode($schema_video_array['description']).',
                        "thumbnailUrl": "'.$schema_video_array['thumbnail']['hqDefault'].'",
                        "uploadDate": "'.$schema_video_array['uploadDate'].'",
                        "duration": "'.$schema_video_array['duration'].'",
                        "embedUrl": "'.$schema_video_array['embedUrl'].'",
                        "videoQuality": "'.$schema_video_array['definition'].'"
                    }
                    </script>';
        }
    }
    ?>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php do_action('falkon_hook_after_body');?>
<!--[if lt IE 8]>
<div class="alert alert-warning">
	You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.
</div>
<![endif]-->
<!-- Main Headers -->
<?php if(is_front_page()) { ?>
<header>
    <?php /* ?><!-- Video Modal -->
    <div class="modal fade" id="videoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content">
                    <button type="button" class="btn btn-default close-video" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/YVr_hfphnDY"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div><?php */ ?>
    <!-- Begin Video container -->
    <div class="video-container">
        <!-- Video -->
        <?php /* ?><video class="feature-video-bg" muted="" loop="" autoplay="" poster="<?php echo get_stylesheet_directory_uri().'/images/home-banner.png';?>">
            <source src="<?php echo get_stylesheet_directory_uri();?>/includes/teaser_2.mp4" type="video/mp4">
        </video><?php */ ?>
        <div id="background-video" class="background-video">
            <img src="<?php echo get_stylesheet_directory_uri();?>/images/placeholder.jpg" alt="" class="placeholder-image">
        </div>
            <!-- Navbar | hme-nar-wrapper has a toggle class for z index with the video overlay-->
            <div class="hme-nav-wrapper">
                <div class="container">
                    <?php get_template_part( 'includes/blocks/hamburger', 'nav' ); ?>
                </div>
            </div>
        <!-- Video overlay with content - overlay brings in black rgba -->
        <div class="video-overlay">
            <div class="container">
                <div class="row header-txt text-center">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <h1>Your Digital Partners</h1>
                    <h3>Working <em class="strong">with</em> you to get great results.</h3>
                    <?php /* ?><div data-toggle="modal" data-target="#videoModal">
                          <i class="fa fa-play-circle-o launch-modal"   aria-hidden="true"></i><br>
                          <a href="#" class="launch-modal" data-modal-id="modal-video"> <span>Watch our video</span></a>
                    </div><?php */ ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- // overlay-->
    </div>
</header>
    <script>
        jQuery(function($) {
            $('#background-video').YTPlayer({
                fitToBackground: true,
                videoId: 'YVr_hfphnDY',
                pauseOnScroll: false,
                callback: function() {
                    videoCallbackEvents();
                }
            });
            var videoCallbackEvents = function() {
                var player = $('#background-video').data('ytPlayer').player;
                player.addEventListener('onStateChange', function(event){
                    console.log("Player State Change", event);
                    // OnStateChange Data
                    if (event.data === 0) {
                        console.log('video ended');
                    }
                    else if (event.data === 2) {
                        console.log('paused');
                    }
                });
            }
        });
    </script>
<?php }
// pages,projects and posts - check for banner meta -> thumbnail -> default
else {
    if ( is_home() or is_author() or is_archive()) {
        global $single_banner_mb;
        $blog_page_id = get_option('page_for_posts');
        $banner_upload = $single_banner_mb->the_meta($blog_page_id);
    }
    else {
        global $single_banner_mb;
        $banner_upload = $single_banner_mb->the_meta();
    }
    $header_block_excerpt = wp_trim_words(get_the_excerpt(), 12, '...' );
    echo '<header>';
            // Backgrounds
            if (is_search()) {
                echo '<div class="inner_header" style="background-image:url(' . get_stylesheet_directory_uri() . '/images/fallback-banner.jpg">';
            }
            elseif(is_404()){
                echo '<div class="inner_header" style="background-image:url(' . get_stylesheet_directory_uri() . '/images/banner-404.jpg">';
            }
            elseif (is_post_type_archive('project')) {
                $banner_bg_array = $falkon_option['falkon_case_banner_image'];
                   foreach ($banner_bg_array as $image_id) {
                        $image_bg_attribute = wp_get_attachment_image_src($image_id, 'full-size');
                        echo '<div class="inner_header" style="background-image:url('.$image_bg_attribute[0].');">';
                   }
            }
            else {
                // check to see if a custom banner image has been uploaded
                if (isset($banner_upload['image_id'])) {
                    $attachment_id = $banner_upload['image_id'];
                    $banner_url = wp_get_attachment_url($attachment_id);
                    echo '<div class="inner_header" style="background-image:url(' . $banner_url . ');">';

                } // no banner uploaded -> check for thumbnail but add an overlay
                elseif (has_post_thumbnail()) {
                    echo '<div class="inner_header_post_thumb" style="background-image:url(' . get_the_post_thumbnail_url() . ');">';
                    echo '<div class="dark_overlay">';
                } // no banner or thumbnail -> default fallback
                else {
            //            echo '<div id="inner-header" style="background-color:#000;">';
                    echo '<div class="inner_header" style="background-image:url(' . get_stylesheet_directory_uri() . '/images/fallback-banner.jpg">';

                }
            }
            echo '<div class="inner-header-shadow"></div>';
?>
<!-- Navbar | hme-nar-wrapper has a toggle class for z index with the video overlay-->
<div class="hme-nav-wrapper">
    <div class="container">
        <?php get_template_part( 'includes/blocks/hamburger', 'nav' ); ?>
    </div>
</div>
<?php
        // Content
            echo '<div class="container">';
                echo '<div class="row header-txt text-center">';
                    echo '<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">';
                    if(is_author()){
                        if (isset($banner_upload['title_line'])) {
                            echo '<h1>' . $banner_upload['title_line'] .' '.__('posts by: ', 'b4st').get_the_author_meta( 'display_name' ).'</h1>';
                        } else {
                            echo the_title('<h1>', '</h1>');
                        }
                        if(get_the_author_meta('description')!=''){
                            echo '<p class="h3">'.get_the_author_meta('description').'</p>';
                        }
                    }
                    elseif(is_category()){
                        $category = get_category( get_query_var( 'cat' ) );
                        $cat_id = $category->cat_ID;
                        $cat_name = $category->cat_name;
                        if (isset($banner_upload['title_line'])) {
                            echo '<h1>' . $banner_upload['title_line'] .' '.__('posts in category: ', 'b4st').$cat_name.'</h1>';
                        } else {
                            echo the_title('<h1>', '</h1>');
                        }
                        if($category->category_description!=''){
                            echo '<p class="h3">'.$category->category_description.'</p>';
                        }
                    }
                    elseif (is_search()) {
                        echo '<h1>You searched for "'.esc_html( get_search_query( false ) ).'" </h1>';
                        echo '<p class="h3">Here are the results:</p>';
                    }
                    elseif(is_404()){
                        echo '<h1>4-OH-F.... FOUR!</h1>';
                        echo '<p class="h3">The page or resource has either been moved, deleted, or much worse...</p>';
                    }
                    elseif (is_post_type_archive('project')) {
                        echo '<h1>'.$falkon_option['falkon_case_study_banner_title'].'</h1>';
                        echo '<h3>'.$falkon_option['falkon_case_study_banner_subtitle'].'</h3>';
                    }
                    else {
                        // title with fallback
                        if (isset($banner_upload['title_line'])) {
                            echo '<h1>' . $banner_upload['title_line'] . '</h1>';
                        } else {
                            echo the_title('<h1>', '</h1>');
                        }
                        if (is_singular('post')) {
                                     if(have_posts()): while(have_posts()): the_post();
                                         echo '<p class="h3">By '. get_the_author() .' on '. get_the_date().'</p>';
                                     endwhile;
                                     else:
                                         echo 'no author';
                                     endif;
                        }
                        else {
                            // sub-title with fallback
                            if (isset($banner_upload['ban_tag'])) {
                                echo '<h3>' . $banner_upload['ban_tag'] . '</h3>';
                            } else {
                                echo '<h3>' . $header_block_excerpt . '</h3>';
                            }
                        }
                    }
                    echo '</div>'; // col
                echo '</div>'; // row
            echo '</div>'; // container
        if ( has_post_thumbnail()) {
            echo '</div>'; // overlay
        }
    echo '</div>'; // inner header
echo '</header>';
}
?>