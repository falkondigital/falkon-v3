<?php
get_header();
?>
<section id="error-page">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
      <div id="content" role="main">
          <h1><?php _e('Looks like you\'re lost?!', 'bst'); ?></h1>
        <p>You
          <?php
          #some variables for the script to use
          #if you have some reason to change these, do.  but wordpress can handle it
          $adminemail = get_option('admin_email'); #the administrator email address, according to wordpress
          $website = get_bloginfo('url'); #gets your blog's url from wordpress
          $websitename = get_bloginfo('name'); #sets the blog's name, according to wordpress

          if (!isset($_SERVER['HTTP_REFERER'])) {
            #politely blames the user for all the problems they caused
            echo "tried going to "; #starts assembling an output paragraph
            $casemessage = "All is not lost!";
          } elseif (isset($_SERVER['HTTP_REFERER'])) {
            #this will help the user find what they want, and email me of a bad link
            echo "clicked a link to"; #now the message says You clicked a link to...
            #setup a message to be sent to me
            $failuremess = "A user tried to go to $website".$_SERVER['REQUEST_URI']." and received a 404 (page not found) error. ";
            $failuremess .= "It wasn't their fault, so try fixing it. They came from ".$_SERVER['HTTP_REFERER'];
            add_filter( 'wp_mail_content_type', 'set_html_content_type' );
            wp_mail($adminemail, "Bad Link To ".$_SERVER['REQUEST_URI'],$failuremess, "From: <$adminemail>"); #email you about problem
            remove_filter( 'wp_mail_content_type', 'set_html_content_type' );
            $casemessage = "That is our fault, and one of our web monkeys has been emailed to fix it ASAP!";#set a friendly message
          }
          echo " ".$website.$_SERVER['REQUEST_URI']; ?>
          and it doesn't exist. <?php echo $casemessage; ?> </p><p>Use the navigation above or <a href="<?php echo esc_url( home_url( '/' ) );?>" title="There's no place like home... There's no place like home... There's no place like home...">click here</a> to get you back on track.
        </p>
      </div><!-- /#content -->
    </div>
  </div><!-- /.row -->
</div><!-- /.container -->
</section>
<?php get_footer(); ?>
