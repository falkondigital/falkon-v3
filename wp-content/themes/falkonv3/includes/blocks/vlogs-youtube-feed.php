<?php
/**
 * Vlogs Youtube feed block
 *
 */
global $falkon_option;

$hp_insta_title = $falkon_option['falkon_vlogs_feed_title']? $falkon_option['falkon_vlogs_feed_title'] : 'Latest VlogsX';
$hp_insta_num = $falkon_option['falkon_vlogs_feed_num']? (int)$falkon_option['falkon_vlogs_feed_num'] : 3;

//$block_location = 'news-homepage';
//$news_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
//echo $news_block;

// Lsatest Vlogs
$output = '<section id="vlogs-youtube-feed" class="pb-6">';
    $output .= '<div class="row">';
    $output .= '<div class="col-xs-12 col-sm-12 col-md-12 text-center">';
        $output .= '<div class="h1-pt">'.$hp_insta_title.'</div>';
        $output .= '<hr class="pink">';
    $output .= '</div>';
    $output .= '</div>';
$output .= '<div class="row">';

// Find posts on instagram and bring them in with a 15 minute "refresh"
$insta_post_result = get_transient( 'vlogs_youtube_feed_transient' );
if ( ($insta_post_result ) === false){
//    _log("GETTING YOUTUBE FEED");
    $api_key=$falkon_option["falkon_vlogs_feed_api_key"];
    $channel_id=$falkon_option["falkon_vlogs_feed_channel_id"];

    $json_link="https://www.googleapis.com/youtube/v3/search";
    $query_args = array(
        'part'      =>  'snippet',
        'channelId' =>  $channel_id,
        'maxResults'=>  $hp_insta_num,
        'order'     =>  'date',
        'type'      =>  'video',
        'key'       =>  $api_key
    );
    $json_link = add_query_arg($query_args,$json_link);
//    _log($json_link);
    $json = file_get_contents($json_link);
//    _log('$json DATA:');
//    _log($json);
    $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
    $insta_post_feed = $obj;
    $expire_time = isset($falkon_option['falkon_blog_vlogs_expire'])?$falkon_option['falkon_blog_vlogs_expire']:DAY_IN_SECONDS;
    set_transient('vlogs_youtube_feed_transient', $insta_post_feed, $expire_time );
}
//if time limit above has not expired - use current variable data :
else {
    $insta_post_feed = $insta_post_result;
}

$grid_space = 12/$hp_insta_num;
$grid_space = $grid_space=='1'?'2':$grid_space;
$xyz = 1;

foreach ($insta_post_feed['items'] as $insta_post) {
//    _log($insta_post);
    $pic_link=$insta_post['id']["videoId"];
//    $pic_like_count=$insta_post['likes']['count'];
//    $pic_comment_count=$insta_post['comments']['count'];

    $output .= '<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-offset-0 col-md-'.$grid_space.' text-center">';
    $output .= '<div class="video-container">
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/'.$pic_link.'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>';
    $output .= '</div>';
    if($xyz++%2 == 0) $output .= '<div class="clearfix visible-xs-block visible-sm-blockX"></div>';
//    $output .= '<div class="visible-xs-block visible-sm-block"></div>';
}
$output .= '</div>';
$output .= '</section>';
echo $output;
