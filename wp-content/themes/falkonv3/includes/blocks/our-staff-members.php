<?php

function falkonstaffmembers( $atts, $content = null ) {

    ob_start();

    $a = shortcode_atts( [
        'contentreturn' => '',
        'title' =>  '',
    ],
        $atts );

    // breakout
    $result = '</div></div></div></section>';
    $result .=   '<section id="grey-page-content" class="our-staff">';
    //Title
    $result .=	'<div class="container">';
    $result .=	'<div class="row">';
    $result .= '<div class="col-xs-12 col-sm-12 text-center">';
            if($atts['title']!='') $result .= '<p class="h2">'.$a['title'].'</p>';
            $result .= '<hr class="pink">';
    $result .=  '</div>';
    $result .=   '</div>';

    $result .=	'<div class="row">';

    //query work
    $args = array(
        'posts_per_page' => '4',
        'post_type' => 'our-people'
    );
    $subpages = new WP_query($args);

    // create output
    if ($subpages->have_posts()) :
    while ($subpages->have_posts()) : $subpages->the_post();
        global $our_work_mb;
        $work_meta = $our_work_mb->the_meta();
            $result .= '<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center">';
            $result .= get_the_post_thumbnail();
            $result .= '<div class="h3">'.get_the_title().'</div>';
            $result .= '</div>';
    endwhile;
    else :
        $result .= '<p>No staff found, sign in and add some.</p>';
    endif;

    $result .=   '</div>';
    $result .= '<div class="row staff-content">';
    $result .= '<div class="col-xs-12 col-sm-12 text-center">';

    // Content
    $result .= wpautop(do_shortcode($content));
    $result .=   '</div>';
    $result .=   '</div>';
    $result .=   '</div>';
    $result .=	 '</section>';

    // BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $result .= '<section id="page-content">
            <div class="container">
                <div class="row">
                     <div class="col-xs-12 col-sm-12 text-center">';
    }

    echo $result;

    // reset the query
    wp_reset_postdata();
    return ob_get_clean();
}
add_shortcode( 'falkon-show-staff', 'falkonstaffmembers' );
