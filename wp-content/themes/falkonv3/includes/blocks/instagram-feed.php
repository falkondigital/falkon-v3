<?php
/**
 * Instagram feed block
 *
 */
global $falkon_option;

$hp_insta_title = $falkon_option['falkon_homepage_instagram_title']? $falkon_option['falkon_homepage_instagram_title'] : 'Behind the ScenesX';
$hp_insta_num = $falkon_option['falkon_homepage_instagram_num']? (int)$falkon_option['falkon_homepage_instagram_num'] : 4;

$hp_insta_user_access_token = $falkon_option['falkon_homepage_instagram_user_access_token']?$falkon_option['falkon_homepage_instagram_user_access_token']:"529466597.1677ed0.5aededdd72e241b3a283d3011c5df18e";

//$hp_insta_num = 2;

//var_dump($hp_insta_title);
//var_dump($hp_insta_num);
//$block_location = 'news-homepage';
//$news_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
//echo $news_block;

$output = '<section id="instagram-feed">';
$output .= '<div class="container">';
    $output .= '<div class="row">';
    $output .= '<div class="col-xs-12 col-sm-12 col-md-12 text-center">';
        $output .= '<div class="h1-pt">'.$hp_insta_title.'</div>';
        $output .= '<hr class="pink">';
    $output .= '</div>';
    $output .= '</div>';
$output .= '<div class="row">';


// Find posts on instagram and bring them in with a 15 minute "refresh"
$insta_post_result = false;//get_transient( 'insta_post_transient' );
if ( ($insta_post_result ) === false){
    $access_token=$hp_insta_user_access_token;

    $json_link_user = "https://graph.instagram.com/me/media";//?fields=id,username";
    $json_link_user_url_args = array(
        "access_token"  =>  $access_token,
        "fields"        =>  "id,caption,media_type,media_url,permalink,thumbnail_url,username",
        "limit"         =>  $hp_insta_num
    );

    $apiUrl_json_link_user = add_query_arg($json_link_user_url_args,$json_link_user);
    $response = wp_remote_get($apiUrl_json_link_user);
    $responseBody = wp_remote_retrieve_body( $response );
    $result = json_decode( $responseBody );
//    var_dump($response);
//    var_dump($responseBody);
//    var_dump($result);
    if ( is_object( $result ) && ! is_wp_error( $result ) ) {
        // Work with the $result data
        //    $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
        $insta_post_feed = $result;

//        var_dump($insta_post_feed);
        $expire_time = isset($falkon_option['falkon_homepage_instagram_expire'])?$falkon_option['falkon_homepage_instagram_expire']:HOUR_IN_SECONDS;
        set_transient('insta_post_transient', $insta_post_feed, $expire_time );
    } else {
        // Work with the error
        _log($result);
    }
}
//if time limit above has not expired - use current variable data :
else {
    $insta_post_feed = $insta_post_result;
}

$grid_space = 12/$hp_insta_num;
$image_size = 'thumbnail';
//thumbnail ~150px
//low_resolution ~320
//standard_resolution ~640
switch($grid_space){
    case '6':
        $image_size = 'standard_resolution';
        break;
    case '4':
        $image_size = 'standard_resolution';
        break;
    case '3':
        $image_size = 'standard_resolution';
        break;
    case '2':
        $image_size = 'low_resolution';
        break;
    case '1':
        $image_size = 'low_resolution';
        break;
    default:
        $image_size = 'thumbnail';
}
$grid_space = $grid_space=='1'?'2':$grid_space;

$xyz = 1;
foreach ($insta_post_feed->data as $insta_post) {
    _log($insta_post);
    $pic_text=$insta_post->caption;
    $pic_link=$insta_post->permalink;
//    $pic_like_count=$insta_post['likes']['count'];
//    $pic_comment_count=$insta_post['comments']['count'];

//    $image_size = 'standard_resolution';
    $pic_src=str_replace("http://", "https://", $insta_post->media_url);

    //Insert image into WP backend to save calls, time and space. (smush it!)
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    require_once( ABSPATH . 'wp-admin/includes/media.php' );

    add_filter( 'upload_dir', 'falkon_change_upload_dir_instagram' );
    $uploads_array = wp_upload_dir();
    remove_filter( 'upload_dir', 'falkon_change_upload_dir_instagram' );

    $uploads_path = $uploads_array['basedir'].'/';
    $uploads_url = $uploads_array['baseurl'].'/';;
    _log($uploads_array);

    $url = $pic_src;
    _log('$url:');
    _log($url);

    // init curl object
//                            $ch = curl_init();
//
//// define options
//                            $optArray = array(
//                                CURLOPT_HEADER => false,
//                                CURLOPT_NOBODY => false,
//                                CURLOPT_RETURNTRANSFER => true,
//                                CURLOPT_FOLLOWLOCATION => true,
//                                CURLOPT_URL => $url,
//                                CURLOPT_SSL_VERIFYPEER => false,
//                            );
//
//// apply those options
//                            curl_setopt_array($ch, $optArray);
//
//// execute request and get response
//                            $result = curl_exec($ch);
//// also get the error and response code
//                            $errors = curl_error($ch);
//                            $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//                            preg_match_all('/^Location:(.*)$/mi', $result, $matches);
//                            curl_close($ch);
//
//                            _log($errors);
//                            _log($response);
//                            _log('$result after curl:');
//                            _log($result);
//                            _log('$matches:');
//                            _log($matches);
//                            echo !empty($matches[1]) ? trim($matches[1][0]) : 'No redirect found';
//                            $url = !empty($matches[1]) ? trim($matches[1][0]) : $result;
//                            _log('$url after messing:');
//                            _log($url);

    $profile_Image = $url;//'http://0.gravatar.com/avatar/a879d103e7fa5a06b4b8a07f336a111c?s=32&d=http%3A%2F%2F0.gravatar.com%2Favatar%2Fad516503a11cd5ca435acc9bb6523536%3Fs%3D32&r=G'; //image url

//                            $imginfo = getimagesize($url);
//                            _log('$imginfo');
//                            _log($imginfo);
//                            _log('$imginfo[mime]');
//                            _log($imginfo['mime']);

//                            $imageData = base64_encode(file_get_contents($profile_Image));
    // Format the image SRC:  data:{mime};base64,{data};
//                            $src = 'data: '.mime_content_type($url).';base64,'.$imageData;
//                            _log('$src:');
//                            _log($src);

//        $userImage = 'ig_hp_feed_'.uniqid().'.jpg'; // renaming image
    $userImage = $insta_post->id.'.jpg'; // renaming image

    _log('$uploads_array[path].$userImage:');
    _log($uploads_array['path'].'/'.$userImage);

    _log(file_exists($uploads_array['path'].'/'.$userImage));

    if(!file_exists($uploads_array['path'].'/'.$userImage)){

        $path = $uploads_path;  // your saving path
        $thumb_image = file_get_contents($profile_Image);

        $thumb_file = $path . $userImage;

        file_put_contents($thumb_file, $thumb_image);

        $url = $uploads_url.$userImage;
        _log('$url after messing:');
        _log($url);
        $tmp = download_url( $url );

        _log('$tmp: download_url');
        if( is_wp_error( $tmp ) ){
            _log("IMAGE ERROR GETTING FROM LOCAL DOWNLOAD".$url." !!");
            _log($tmp->get_error_message());
            // download failed, handle error
            @unlink($tmp);
            continue;
        }
        else{
            _log($tmp);
            $desc = $insta_post->id;
            $file_array = array();

            // Set variables for storage
            // fix file filename for query strings
            preg_match('/[^\?]+\.(jpg|jpe|jpeg|gif|png)/i', $url, $matches);
            $file_array['name'] = basename($matches[0]);
            $file_array['tmp_name'] = $tmp;

            $post_information = array(
                'post_parent'	=>	0,
                'menu_order'	=>	0,
                'post_content'  =>  $insta_post->caption,
                'post_author'   =>  0,
            );

            add_filter( 'upload_dir', 'falkon_change_upload_dir_instagram' );
            add_filter( 'intermediate_image_sizes_advanced', 'my_remove_default_image_sizes' );

            // do the validation and storage stuff
            $id = media_handle_sideload( $file_array, 0, $desc,$post_information );

            remove_filter( 'upload_dir', 'falkon_change_upload_dir_instagram' );
            remove_filter( 'intermediate_image_sizes_advanced', 'my_remove_default_image_sizes' );

            _log('$id media_handle_sideload:');
//
            // If error storing permanently, unlink
            if ( is_wp_error($id) ) {
                @unlink($file_array['tmp_name']);
                _log('error storing permanently, unlink:');
                _log($id->get_error_message());
            }
            else{
                _log($id);
//                _log('LOG SIZES:');
//                _log(get_intermediate_image_sizes());
//                $pic_src = wp_get_attachment_url( $id,'thumbnail-large' );
                $pic_src = wp_get_attachment_image_src( $id,'thumbnail-small' );
                $pic_src = $pic_src[0];
                _log('$src: wp_get_attachment_url');
                _log($pic_src);
                $auction_img1[] = $id;
                //http://192.168.0.102/dealerbikes/wp-content/uploads/2015/10/logo-aprilia1.jpg|6440,
                //@unlink($result);
            }
        }
        @unlink($thumb_file);
    }
    else{
        $userImage = $insta_post->id.'-270x270.jpg'; // renaming image
        if(!file_exists($uploads_array['path'].'/'.$userImage)){
            $userImage = $insta_post->id.'.jpg'; // renaming image
        }
        $pic_src = $uploads_array['url'].'/'.$userImage;
        $id = get_attachment_id($pic_src);
    }

    $image_id = $id;
    $img_src = wp_get_attachment_image_url( $image_id, 'thumbnail-small' );
    $img_srcset = wp_get_attachment_image_srcset( $image_id,'full' );
//    var_dump($img_srcset);
//    $img_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );

    $image_attributes = wp_get_attachment_image_src($image_id, 'full');
    $img_meta       = wp_prepare_attachment_for_js($image_id);
    $image_title    = $insta_post->caption;
    $image_alt      = $img_meta['alt'] == '' ? 'Instagram image post '.$insta_post->id.' from user '.$insta_post->username: $img_meta['alt'];

    $img_arg_array = array(
        "title" =>  $image_title,
        "alt" =>  $image_alt,
        "class" => "img-responsive feedimg"
    );
//    $output .= 'HHEEELOOOO<img class="wp-image-10158" src="'.esc_attr( $img_src ).'" srcset="'.esc_attr( $img_srcset ).'" sizes="100vw" alt="'.$img_alt.'">';

//    $pic_created_time=date("F j, Y", $insta_post['caption']['created_time']);
//    $pic_created_time=date("F j, Y", strtotime($pic_created_time . " +1 days"));
    $output .= '<div class="col-xs-6 col-sm-6 col-md-'.$grid_space.'">';
    $output .= '<a href="'.$insta_post->permalink.'" rel="nofollow" target="_blank">';
    $output .= wp_get_attachment_image( $image_id, 'thumbnail-small',"", $img_arg_array);
//    $output .= '<img src="'.$pic_src.'" title="'.esc_attr($insta_post['caption']['text']).'" alt="Instagram image post '.$insta_post['id'].' from user '.$insta_post['user']['full_name'].'" class="feedimg" />';
//    $output .= '<img src="'.$pic_src.'" srcset="'.esc_attr( $img_srcset ).'" sizes="100vw" title="'.esc_attr($insta_post['caption']['text']).'" alt="Instagram image post '.$insta_post['id'].' from user '.$insta_post['user']['full_name'].'" class="feedimg" />';
    $output .= '</a>';
    $output .= '</div>';
    if($xyz++%2 == 0) $output .= '<div class="clearfix visible-xs-block visible-sm-block"></div>';
//    $output .= '<div class="visible-xs-block visible-sm-block"></div>';
}
$output .= '</div>';
$output .= '</div>';
$output .= '</section>';
echo $output;
