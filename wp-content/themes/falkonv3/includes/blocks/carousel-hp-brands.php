<?php $slide_num = 4; ?>
<?php
// Falkon Options
global $falkon_option;

if(isset($falkon_option['falkon_homepage_client_images'])) {
    $result = '<section id="clients-logo-carousel" class="">';
//        $result .= '<div class="container">';
    $result .= '<div class="row justify-content-center">';
    $result .= '<div class="col-12">';

    $thumb_array = $falkon_option['falkon_homepage_client_images'];
    //    var_dump($thumb_array);
    $result .= '<div id="carousel-client" class="owl-carousel owl-theme">';
    foreach ($thumb_array as $image_id) {
        $image_attributes = wp_get_attachment_image_src($image_id, 'full-size');
//                        var_dump($image_attributes);
        $img_meta       = wp_prepare_attachment_for_js($image_id);
        $image_title    = $img_meta['title'] == '' ? '' : $img_meta['title'];
        $image_alt      = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];

//                        var_dump($img_meta['date']);
//                        var_dump(date('Y/m',($img_meta['date']/1000)));
//                        var_dump(wp_upload_dir(date('Y/m',($img_meta['date']/1000))));
        $retina_image = '';
        $info = get_attached_file($image_id);

//                        var_dump($info);

        $source_dirname  = pathinfo($info, PATHINFO_DIRNAME);
        $source_filename = pathinfo($info, PATHINFO_FILENAME);
        $source_ext      = pathinfo($info, PATHINFO_EXTENSION);

//                        var_dump($source_dirname);
//                        var_dump($source_filename);
//                        var_dump($source_ext);

        $file_explode = explode('.',$info);
//                        var_dump($file_explode);
//                        var_dump(file_exists($source_dirname.$source_filename.'@2x.'.$source_ext));
        if (file_exists(trailingslashit($source_dirname).$source_filename.'@2x.'.$source_ext)){
            $wp_upload_dir_image = wp_upload_dir(date('Y/m',($img_meta['date']/1000)));
            $retina_image = 'data-src-retina="'.trailingslashit($wp_upload_dir_image['url']).$source_filename.'@2x.'.$source_ext.'"';

        }
        $result .= '<div class="item">';
        $result .= '<img class="owl-lazy"
                        data-src="' . $image_attributes[0] . '"
                        '.$retina_image.'
                        alt="'.$image_alt.'"
                        >'; //src="' . $image_attributes[0] . '"
        $result .= '</div>';
    }
    $result .= '</div>';
    $result .= '</div>';
    $result .= '<div class="clearfix"></div><div id="customNavContainer" class="col-sm-12"></div>';

    $result .= '</div>';
//            $result .= '</div>';
    $result .= '</section>';
    echo $result;
}
?>
<?php
$client_num = count($thumb_array);
$carousel_loop = $client_num>$slide_num?'true':'false';
$mouse_drag = $client_num>$slide_num?'true':'false';
$touch_drag = $client_num>$slide_num?'true':'false';
?>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        var owlBrand = $('#carousel-client');
        owlBrand.owlCarousel({
            center: false,
            margin:30,
//                center:true,
            nav:true,
            navContainer:'#customNavContainer',
            navText:['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
            dots:false,
            lazyLoad:true,
            lazyLoadEager:1,
//                loop:true,
            loop: <?php echo $carousel_loop;?>,
            mouseDrag:<?php echo $mouse_drag;?>,
            touchDrag:<?php echo $touch_drag;?>,
            autoplay: <?php echo $carousel_loop;?>,
//                autoplayTimeout:6000,
//                autoplayTimeout: 7000,
            autoplayHoverPause: true,
            autoplaySpeed: 1500,
//                dotsSpeed: 1500,
            responsive:{
                0:{
                    items:2
                },
                577:{
                    items:4
                },
                991:{
                    items:4
                }
            }
        });
    });
</script>
