<div id="grey-page-content">
    <div class="container">
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

            <?php
            echo '	<h3 class="related-title">Related posts</h3>';
            echo '	<hr class="pink">';
            $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
            if( $related ) foreach( $related as $post ) {
                setup_postdata($post); ?>



                <div class="col-xs-12 col-sm-4">
                    <div class="col-xs-12 col-sm-12 post-holder np">
                        <article role="article" id="post_<?php the_ID()?>">
                            <div class="text-center">

                                <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 20, '...' ); ?>

                                <!-- Post Thumbnail -->
                                <?php get_template_part('includes/blocks/blog-feed', 'thumbnail'); ?>


                                <div class="post-content">
                                    <div class="post-title">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <h4><?php the_title(); ?></h4>
                                        </a>
                                    </div>

                                    <!--            <div class="entry-meta">-->
                                    <!--                --><?php //flkn_news_posted_on(); ?>
                                    <!--            </div><!-- .entry-meta -->
                                    <div class="post-exrt">
                                        <p><?php echo $block_excerpt; ?></p>
                                    </div>


                                    <?php //flkn_read_more();?>
                                </div>

                            </div>

                        </article>
                    </div>
                </div>
            <?php }
            wp_reset_postdata(); ?>

        </div>

    </div>
</div>
