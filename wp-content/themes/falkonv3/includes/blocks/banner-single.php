<div id="single-banner">
<!-- Post Thumbnail -->
<?php if(has_post_thumbnail($post->ID)){

    $source = $post -> ID;
    $imgSRC = get_the_post_thumbnail_url($source,'full');


    $result = '<div style="background-image:url('.$imgSRC.'); background-position:center center; background-repeat:no-repeat; background-size:cover; position:relative; height:315px; opacity: 0.30;"/>';
    $result .= '</div>';
    $result .= '<div class="container">';
        $result .= '<h1>'.get_the_title() .'</h1>';
    $result .= '</div>';

    echo $result;

}
else{
    $args = array(
        'numberposts' => 1,
        'order'=> 'ASC',
        'post_mime_type' => 'image',
        'post_parent' =>  $post->ID,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'orderby' => 'menu_order ID'
    );
    $attachments = get_children( $args );
    if ($attachments) {
        foreach($attachments as $attachment) {
            $imgSRC = wp_get_attachment_image_src( $attachment->ID, 'thumbnail-blog' );
            $background = '<div style="background-image:url('.$imgSRC[0].'); background-position:center center; background-repeat:no-repeat; background-size:cover; position:relative; height:315px; opacity: 0.30;"/>';
        }

        $result = $background;
        $result .= '</div>';
        $result .= '<div class="container">';
            $result .= '<h1>'.get_the_title() .'</h1>';
        $result .= '</div>';

        echo $result;

    }

    else{


        $result = '<div style="background-image:url('.get_template_directory_uri().'/images/fallback-banner.jpg'.'); background-position:center center; background-repeat:no-repeat; background-size:cover; position:relative; height:315px; opacity: 0.30;"/>';
        $result .= '</div>';
        $result .= '<div class="container">';
            $result .= '<h1>'.get_the_title() .'</h1>';
        $result .= '</div>';

        echo $result;

    }
}
?>
</div>