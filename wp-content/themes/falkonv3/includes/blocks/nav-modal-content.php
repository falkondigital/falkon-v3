<?php
/**
 *
 */
global $falkon_option;

//wp_nav_menu( array(
//        'theme_location'    => 'main-nav',
//        'menu' => 'main-nav',
//        'depth'             => 1,
//        'menu_class'        => '',
//        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
////        'after'           => '<i class="fa fa-angle-down subclicker"></i>',
//        'walker'            => new wp_bootstrap_navwalker())
//);

$login_link = '';
if ( is_user_logged_in() ) {
    $login_link = '<li><a href="'.wp_logout_url().'" title="'.__('Log out','woothemes').'" class="">'.__('Log out','woothemes').'</a></li>';
}
else {
    $login_link = '';
}

$menu_name = 'main-nav';
$menu_args = array(
    'theme_location'    => $menu_name,
    'menu' => 'main-nav',
    'depth'             => 1,
    'menu_class'        => '',
    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
//        'after'           => '<i class="fa fa-angle-down subclicker"></i>',
    'container' => false,
    'items_wrap'      => '%3$s',
    'walker'            => new wp_bootstrap_navwalker()
);
echo '<ul id="menu-header-navigation" class="header-menu">'.yourtheme_nav_menu_args($menu_name,$menu_args).$login_link.'</ul>';
echo '<hr>';
echo '<div class="flkn-number">'.$falkon_option['falkon_company_phone_no'].'</div>';

$social_links_header = $falkon_option['falkon_multicheckbox_inputs'];
if(count($social_links_header)>0){
    foreach($social_links_header as $social_url => $value){
        //				var_dump($value);
        //var_dump($falkon_option[$social_url]);
        if($value and $falkon_option[$social_url]!='') echo '<a href="'.esc_url($falkon_option[$social_url]).'" class="sociallink2" rel="nofollow" target="_blank"><i class="ss ss1 fa fa-lg fa-custom-'.$social_url.'"></i><i class="ss ss2 fa fa-lg fa-custom-'.$social_url.'"></i></a>';
    }
}
?>