<?php
/**
 * Related work
 */
global $falkon_option;
global $our_work_mb;
$related_meta_items = $our_work_mb->the_meta();
//var_dump($falkon_option);
//var_dump($related_meta_items);
$currentId = $post->ID;
$related_items = isset($related_meta_items['related_items'])?(array)$related_meta_items['related_items']:array();
$custom_post = 'project';
$custom_post_object = get_post_type_object( $custom_post );
$custom_post_name = $custom_post_object->labels->name;
$xyz=0;


$number_of_related_posts = $falkon_option['falkon_related_studies_number'];

//var_dump($falkon_option);
//var_dump($number_of_related_posts);

$exisiting_posts = array();
$exisiting_posts[] = $currentId;
$exisiting_posts = array_merge($related_items,$exisiting_posts);

if(count($related_items)<$number_of_related_posts){
	$args = array(
		'posts_per_page'    =>	($number_of_related_posts-count($related_items)),
		'post_type'         =>  $custom_post,
		'post_status'	    =>  'publish',
		'post__not_in'      =>  $exisiting_posts,
		'orderby'		    => 'rand',
		'fields'            =>  'ids'
	);
	$extra_posts = new WP_Query( $args );
	if($extra_posts->have_posts()) {
		$related_items = array_merge($related_items,$extra_posts->posts);
		wp_reset_postdata();
	}
}

$args = array(
	'post_type'         =>  $custom_post,
	'post_status'	    =>  'publish',
	'post__in'          =>  $related_items,
	'orderby'		    => 'post__in',
	'posts_per_page'    =>	$number_of_related_posts,
);
$related_query = new WP_Query( $args );


if($related_query->have_posts()){
	$related_posts_title = $falkon_option['falkon_related_studies_title'];


    $result =  '<section id="work-thumbs">';
    $result .= '<div class="container-fluid np">';
        $result .= '<div class="row">';
            $result .= '<div class="col-xs-12 col-sm-12 text-center">';
                $result .= '<h2>'.$related_posts_title.'</h2>';
                $result .= '<hr class="pink">';
            $result .= '</div>';
        $result .= '</div>';

    $result .= '<div class="row">';

	while ( $related_query->have_posts() ) :
		$related_query->the_post();

		//Related work output

        global $our_work_mb;
        $current_related_item_meta = $our_work_mb->the_meta( $related_query->ID);

//        var_dump($current_related_item_meta);
        $current_item_subtitle = '';
        if (isset($work_meta['hover_tagline'])) {
            $current_item_subtitle = $work_meta['hover_tagline'];
        }
        else {
            $terms =  get_the_terms( $related_query->ID , 'our-case-type' );
            foreach ( $terms as $term ) {
                $current_item_subtitle =  $term->name;
                break;
            }
            $current_item_subtitle = ($current_item_subtitle!=''?$current_item_subtitle:'&nbsp;');
        }

        $result .= '<a href="'. get_the_permalink().'" title="'.esc_attr(get_the_title().($current_item_subtitle!='&nbsp;'?' - '.$current_item_subtitle:' - View Case Study')).'">';
        $result .= '<div class="col-xs-12 col-sm-6 col-md-6 bg_hold np" style="background-image: url('.get_the_post_thumbnail_url().'">';
        $result .= '<div class="hidden-hover">';
        $result .= '<div class="overlay-title">'.get_the_title().'</div>';
        $result .= '<div class="overlay-sub-title">'.$current_item_subtitle.'</div>';
        $result .= '<a href="'.get_the_permalink().'" class="btn button ghost-btn" title="View Case Study">View Case Study</a>';
        $result .= '</div>';
        $result .= '</div>';
        $result .= '</a>';

	endwhile;

    $result .= '</div>';
    $result .= '</section>';

    echo $result;

	wp_reset_postdata();
}