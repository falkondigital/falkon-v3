<?php

function falkonworktags() {

    ob_start();
    $post = get_post();
//    var_dump($post);
$output = '<div id="project-features">';
$output .= '<div class="h3">Features</div>';
$terms =  get_the_terms( $post->ID , 'our-case-type' );
//var_dump($terms);
$output .=  '<ul>';
         foreach ( $terms as $term ) {
            $output .=  '<li>'.$term->name.'</li>';
         }
$output .=  '</ul>';
$output .=  '</div>';


echo $output;

return ob_get_clean();
}
add_shortcode( 'work-features', 'falkonworktags' );


function falkonworkshowcase( $atts ) {

    ob_start();

    $a = shortcode_atts( [
        'showcase_img'=> false,
        'contentreturn' =>'',
        'bg_img' => false,
        'font_color' => '',
        'title' =>'',
    ],
        $atts );

//// Get the image and the image's alt value.
    $image     = wp_get_attachment_image_src( $a['showcase_img'],'full' );
    $bg_image     = wp_get_attachment_image_src( $a['bg_img'],'full' );

//// |Output Without Image ID|
    if ( ! $a['showcase_img'] ) {
        echo 'hey no image?';
    }

// |Output With Image ID|
// BREAKOUT
    $output = '</div></div></div></section>';

    $output .= '<section id="our-work-showcase" class="showcase_bg" style="background-image:url('.$bg_image[0].'); color:'.$a['font_color'].'!important;">';
    $output .= '<div class="container">';
        $output .= '<div class="row title-row">';
            $output .= '<div class="col-xs-12 col-sm-12 text-center">';
            $output .= '<h3>'.$a['title'].'</h3>';
            $output .= '</div>';
            $output .= '</div>';

            $output .= '<div class="row">';
        //    $output .= '<div class="col-xs-12 col-sm-12 col-md-3 showcase-txt">';
        //    $output .= wpautop(do_shortcode($content));
        //    $output .= '</div>';
            $output .= '<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">';
            $output .= '<img src="'.$image[0].'" class="showcase-img">';
            $output .= '</div>';
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</div>';


    $output .= '</section>';

// END RESULTS

// BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $output .= '<section id="page-content">
            <div class="container">
                <div class="row">
                     <div class="col-xs-12 col-sm-12 text-centerrelated-work.php">';
    }
    echo $output;
    return ob_get_clean();
}
add_shortcode( 'falkon-showcase', 'falkonworkshowcase' );


?>