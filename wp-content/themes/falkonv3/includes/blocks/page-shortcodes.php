<?php
function falkonlogogrid( $atts ) {
ob_start();

    $att_array = shortcode_atts( [
        'cms_logo_1'=> false,
        'cms_logo_2'=> false,
        'cms_logo_3'=> false,
        'cms_logo_4'=> false,
//        'banner_height' => '',
        'contentreturn' => '',
    ],
        $atts );

// Get the image and the image's alt value.
//    $logo_one     = wp_get_attachment_image_src( $att_array['cms_logo_1'],'full' );
//    $logo_two     = wp_get_attachment_image_src( $att_array['cms_logo_2'],'full' );
//    $logo_three     = wp_get_attachment_image_src( $att_array['cms_logo_3'],'full' );
//    $logo_four     = wp_get_attachment_image_src( $att_array['cms_logo_4'],'full' );

// BREAKOUT
$output = '</div></div></div></section>';

    $output .= '<section id="grey-page-content">';
    $output .= '<div class="container">';
    $output .= '<div class="row text-center" id="falkon_cms_logos_wrapper">';
// BEGIN CMS LOGOS
    foreach ($att_array as $image_id) {
        $image_attributes = wp_get_attachment_image_src((int)$image_id, 'full');
        if($image_attributes!==false) {
            $img_meta = wp_prepare_attachment_for_js($image_id);
            $image_title = $img_meta['title'] == '' ? '' : $img_meta['title'];
            $image_alt = $img_meta['alt'] == '' ? $image_title : $img_meta['alt'];

            $retina_image = '';
            $info = get_attached_file($image_id);
            $source_dirname = pathinfo($info, PATHINFO_DIRNAME);
            $source_filename = pathinfo($info, PATHINFO_FILENAME);
            $source_ext = pathinfo($info, PATHINFO_EXTENSION);

            if (file_exists(trailingslashit($source_dirname) . $source_filename . '@2x.' . $source_ext))
                $retina_image = " data-retina";

            $output .= '<div class="col-xs-12 col-sm-3">';
            $output .= '<img src="' . $image_attributes[0] . '" title="' . esc_attr($image_title) . '" alt="' . esc_attr($image_alt) . '" ' . $retina_image . '>';
            $output .= '</div>';
        }

    }
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</section>';


//
//                $output .= '<div class="col-xs-12 col-sm-3">';
//                     $output .= '<img src="'.$logo_two[0].'">';
//                $output .= '</div>';
//                $output .= '<div class="col-xs-12 col-sm-3">';
//                    $output .= '<img src="'.$logo_three[0].'">';
//                $output .= '</div>';
//                $output .= '<div class="col-xs-12 col-sm-3">';
//                    $output .= '<img src="'.$logo_four[0].'">';
//                $output .= '</div>';





// END RESULTS

// BACK IN {-if set to yes-}
if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $output .= '<section id="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">';

}

 echo $output;

return ob_get_clean();
}
add_shortcode( 'falkon_cms_logos', 'falkonlogogrid' );


// left/right images
function falkon_two_column_image_choiceX( $atts,  $content = null ) {
    ob_start();
    $a = shortcode_atts( [
        'image_position' => '',
        'image_bg_id' => '',
        'img_height' => '',
        'paddingtop' => '',
        'paddingbottom' => '',
        'opt_class' => '',
        // format
        'breakout' => '',
        'content-return' =>''
    ],
        $atts );

    // Get the image and the image's alt value.
    $image     = wp_get_attachment_image_src( $a['image_bg_id'],'full' );

    // padding
    if ($a['paddingtop']) {
        $paddingtop = 'padding-top:'.$a['paddingtop'].'px;';
    }

    if ($a['paddingbottom']) {
        $paddingbottom = 'padding-bottom:'.$a['paddingbottom'].'px;';
    }


    // Breakout or not | might not have to breakout if a SC has been used above...
    if ( $a['breakout'] == 'Yes' || $a['breakout'] == 'yes' ) {
        $output = '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</section>';
        $output .= '<section id="custom-two-col-sc" class="'.$a['opt_class'].'" style="'.$paddingtop.' '.$paddingbottom.'">';
    }
    else {
        $output = '<section id="custom-two-col-sc" class="'.$a['opt_class'].'" style="'.$paddingtop.' '.$paddingbottom.'">';
    }

    // height management
    if ($a['img_height']) {
        $bgimgheight = $a['img_height'];
    }
    else {
        $bgimgheight = '600';
    }


    $output .= '<div class="container-fluid">';
    $output .= '<div class="row">';
    if ( strtolower($a['image_position']) == 'left' ) {

        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 two-col-img" style="background-image:url('.$image[0].'); height:'.$bgimgheight.'px;">';
        $output .= '</div>';

        $output .= '<div class="col-xs-12 col-sm-12 col-md-6">';
        $output .= '<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 two-col-cntent">';
        $output .= wpautop(do_shortcode($content));
        $output .= '</div>';
        $output .= '</div>';
    }
    elseif ( strtolower($a['image_position']) == 'right' ){

        $output .= '<div class="col-xs-12 col-sm-12 col-md-6">';
        $output .= '<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10 col-lg-offset-4 col-lg-8 two-col-cntent">';
        $output .= wpautop(do_shortcode($content));
        $output .= '</div>';
        $output .= '</div>';

        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 two-col-img" style="background-image:url('.$image[0].'); height:'.$bgimgheight.'px;">';
        $output .= '</div>';

    }
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</section>';

// BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $output .= '<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">';
    }

    echo $output;
    return ob_get_clean();
}
add_shortcode( 'two_column_content', 'falkon_two_column_image_choiceX' );


// left/right images
function falkon_two_column_image_choice( $atts,  $content = null ) {
    ob_start();
    $a = shortcode_atts( [
        'image_position' => '',
        'image_bg_id' => '',
        'img_height' => '',
        'paddingtop' => '',
        'paddingbottom' => '',
        'opt_class' => '',
        // format
        'breakout' => '',
        'content-return' =>''
    ],
        $atts );

    // Get the image and the image's alt value.
    $image     = wp_get_attachment_image_src( $a['image_bg_id'],'full' );

    // padding
    if ($a['paddingtop']) {
        $paddingtop = 'padding-top:'.$a['paddingtop'].'px;';
    }

    if ($a['paddingbottom']) {
        $paddingbottom = 'padding-bottom:'.$a['paddingbottom'].'px;';
    }


    // Breakout or not | might not have to breakout if a SC has been used above...
    if ( $a['breakout'] == 'Yes' || $a['breakout'] == 'yes' ) {
        $output = '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</section>';
        $output .= '<section id="custom-two-col-sc" class="'.$a['opt_class'].'" style="'.$paddingtop.' '.$paddingbottom.'">';
    }
    else {
        $output = '<section id="custom-two-col-sc" class="'.$a['opt_class'].'" style="'.$paddingtop.' '.$paddingbottom.'">';
    }

    // height management
    if ($a['img_height']) {
        $bgimgheight = $a['img_height'];
    }
    else {
        $bgimgheight = '600';
    }


    $output .= '<div class="container">';
    $output .= '<div class="row vertical-align">';
    if ( strtolower($a['image_position']) == 'left' ) {
        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 text-right align-self-center mega-image" >';
        $output .= '<img src="'.$image[0].'" />';
        $output .= '</div>';

        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 text-justify">';
//        $output .= '<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 two-col-cntent">';
        $output .= wpautop(do_shortcode($content));
//        $output .= '</div>';
        $output .= '</div>';
    }
    elseif ( strtolower($a['image_position']) == 'right' ){
        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 col-sm-push-6 text-left align-self-center mega-image">';
        $output .= '<img src="'.$image[0].'" />';
        $output .= '</div>';

//        $output .= '<div class="col-xs-12 col-sm-12 col-md-6">';
        $output .= '<div class="col-xs-12 col-sm-12 col-md-6 col-sm-pull-6 text-justify">';
        $output .= wpautop(do_shortcode($content));
//        $output .= '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</section>';

// BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $output .= '<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">';
    }

    echo $output;
    return ob_get_clean();
}
add_shortcode( 'two_column_content', 'falkon_two_column_image_choice' );

?>