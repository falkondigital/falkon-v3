<?php
/**
 * Instagram feed block
 *
 */
global $falkon_option;

$hp_insta_title = $falkon_option['falkon_homepage_instagram_title']? $falkon_option['falkon_homepage_instagram_title'] : 'Behind the ScenesX';
$hp_insta_num = $falkon_option['falkon_homepage_instagram_num']? (int)$falkon_option['falkon_homepage_instagram_num'] : 2;
//$hp_insta_num = 2;

//var_dump($hp_insta_title);
//var_dump($hp_insta_num);
//$block_location = 'news-homepage';
//$news_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
//echo $news_block;

$output = '<section id="instagram-feed">';
$output .= '<div class="container">';
    $output .= '<div class="row">';
    $output .= '<div class="col-xs-12 col-sm-12 col-md-12 text-center">';
        $output .= '<div class="h1-pt">'.$hp_insta_title.'</div>';
        $output .= '<hr class="pink">';
    $output .= '</div>';
    $output .= '</div>';
$output .= '<div class="row">';

// Find posts on instagram and bring them in with a 15 minute "refresh"
$insta_post_result = false;//get_transient( 'insta_post_transient' );
if ( ($insta_post_result ) === false){
    $access_token="529466597.1677ed0.5aededdd72e241b3a283d3011c5df18e";
    $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
    $json_link.="access_token={$access_token}&count={$hp_insta_num}";
//    $json = file_get_contents($json_link);
//    _log('$json DATA:');
//    _log($json);
//    $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
    $json = file_get_contents($json_link);
    $obj = json_decode(preg_replace('/("\w+"):(\d+)/', '\\1:"\\2"', $json), true);
    $insta_post_feed = $obj;
    $expire_time = isset($falkon_option['falkon_homepage_instagram_expire'])?$falkon_option['falkon_homepage_instagram_expire']:1800;
    set_transient('insta_post_transient', $insta_post_feed, $expire_time );
}
//if time limit above has not expired - use current variable data :
else {
    $insta_post_feed = $insta_post_result;
}
$grid_space = 12/$hp_insta_num;
$image_size = 'thumbnail';
//thumbnail ~150px
//low_resolution ~320
//standard_resolution ~640
switch($grid_space){
    case '6':
        $image_size = 'standard_resolution';
        break;
    case '4':
        $image_size = 'standard_resolution';
        break;
    case '3':
        $image_size = 'standard_resolution';
        break;
    case '2':
        $image_size = 'low_resolution';
        break;
    case '1':
        $image_size = 'low_resolution';
        break;
    default:
        $image_size = 'thumbnail';
}
$grid_space = $grid_space=='1'?'2':$grid_space;

$xyz = 1;
foreach ($insta_post_feed['data'] as $insta_post) {
    $pic_text=$insta_post['caption']['text'];
    $pic_link=$insta_post['link'];
//    $pic_like_count=$insta_post['likes']['count'];
//    $pic_comment_count=$insta_post['comments']['count'];
    $pic_src=str_replace("http://", "https://", $insta_post['images'][$image_size]['url']);
//    $pic_created_time=date("F j, Y", $insta_post['caption']['created_time']);
//    $pic_created_time=date("F j, Y", strtotime($pic_created_time . " +1 days"));
    $output .= '<div class="col-xs-6 col-sm-6 col-md-'.$grid_space.'">';
    $output .= '<a href="'.$insta_post['link'].'" rel="nofollow" target="_blank">';
    $output .= '<img src="'.$pic_src.'" title="'.esc_attr($insta_post['caption']['text']).'" alt="Instagram image post '.$insta_post['id'].' from user '.$insta_post['user']['full_name'].'" class="feedimg" />';
    $output .= '</a>';
    $output .= '</div>';
    if($xyz++%2 == 0) $output .= '<div class="clearfix visible-xs-block visible-sm-block"></div>';
//    $output .= '<div class="visible-xs-block visible-sm-block"></div>';
}
$output .= '</div>';
$output .= '</div>';
$output .= '</section>';
echo $output;
