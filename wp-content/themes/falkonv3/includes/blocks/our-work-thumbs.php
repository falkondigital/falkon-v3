<?php
/**
 * Case Studies / Our Work Homepage thumbnail block
 */
$result  = '<section id="work-thumbs">';
$result .= '<div class="container">';
$result .= '<div class="row">';
$result .= '<div class="col-xs-12 col-sm-12 text-center">';
$result .= '<h2>Some of our featured work</h2>';
$result .= '<hr class="pink">';
$result .= '</div>';
$result .= '</div>';
$result .= '</div>';
$result .= '<div class="container-fluid">';
$result .= '<div class="row">';

//query work
$args = array(
    'posts_per_page' => '6',
    'post_type' => 'project'
);
$subpages = new WP_query($args);
// create output
if ($subpages->have_posts()) :
while ($subpages->have_posts()) : $subpages->the_post();
    global $our_work_mb;
    $work_meta = $our_work_mb->the_meta($subpages->ID);
    $current_item_subtitle = '';
    if (isset($work_meta['hover_tagline'])) {
        $current_item_subtitle = $work_meta['hover_tagline'];
    }
    else {
        $terms =  get_the_terms( $subpages->ID , 'our-case-type' );
        foreach ( $terms as $term ) {
            $current_item_subtitle =  $term->name;
            break;
        }
        $current_item_subtitle = ($current_item_subtitle!=''?$current_item_subtitle:'&nbsp;');
    }

    $result .= '<a href="'. get_the_permalink().'" title="'.esc_attr(get_the_title().($current_item_subtitle!='&nbsp;'?' - '.$current_item_subtitle:' - View Case Study')).'">';
        $result .= '<div class="col-xs-12 col-sm-6 col-md-6 bg_hold np" style="background-image: url('.get_the_post_thumbnail_url().'">';
            $result .= '<div class="hidden-hover">';
                $result .= '<div class="overlay-title">'.get_the_title().'</div>';
                $result .= '<div class="overlay-sub-title">'.$current_item_subtitle.'</div>';
                $result .= '<a href="'.get_the_permalink().'" class="btn button ghost-btn" title="View Case Study">View Case Study</a>';
            $result .= '</div>';
        $result .= '</div>';
    $result .= '</a>';
endwhile;
else :
    $result .= '<p>No Case studies found, sign in and add some.</p>';
endif;

$result .=   '</div>';
$result .=   '</div>';
$result .=	 '</section>';
echo $result;

// reset the query
wp_reset_postdata();
?>