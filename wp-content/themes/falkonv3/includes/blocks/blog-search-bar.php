<div id="search-blog-flk">
    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-offset-6 col-md-6">
                <div class="row text-right">
                <div class="col-xs-12 col-sm-6 col-md-4 cat-dropdown">
                    <?php wp_dropdown_categories( 'show_option_none=Categories' ); ?>
                    <script type="text/javascript">

                        var dropdown = document.getElementById("cat");
                        function onCatChange() {
                            if ( dropdown.options[dropdown.selectedIndex].value > 0 ) {
                                location.href = "<?php echo esc_url( home_url( '/' ) ); ?>?cat="+dropdown.options[dropdown.selectedIndex].value;
                            }
                        }
                        dropdown.onchange = onCatChange;
                    </script>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-8 search-field">
                    <?php get_search_form();?>
                </div>
                </div>
            </div>
    </div>
</div>