<?php if(has_post_thumbnail($post->ID)){
    $source = $post -> ID;
    $is_stick = is_sticky($post->ID);
//    $imgSRC = get_the_post_thumbnail_url($source,'full');
    $image_id = get_post_thumbnail_id($post->ID);
    $result = '<a href="'. get_the_permalink() .'" rel="nofollow" title="'.esc_attr(get_the_title()).'">';
//    <div style="background-image:url('.$imgSRC.');" class="blog-single-thumbnail"/>';
//    $result .= '</div>'

    $image_attributes = wp_get_attachment_image_src($image_id, 'full');
    if($image_attributes!==false){
        $img_meta       = wp_prepare_attachment_for_js($image_id);
        $image_title    = '';
        $image_alt      = $img_meta['alt'] == '' ? 'Falkon blog post featured image for post - '.esc_attr(get_the_title()) : $img_meta['alt'];

        $img_arg_array = array(
            "title" =>  $image_title,
            "alt" =>  $image_alt,
            "class" => "img-responsive blog-single-thumbnail-img"
        );
        $result .= wp_get_attachment_image( $image_id, ($is_stick?'full':'medium'),"", $img_arg_array);
    }
    else{
        $result = '<img src="'.get_template_directory_uri().'/images/fallback-banner-768x352.jpg" class="blog-single-thumbnail-img" alt="default blog thumbnail image"/>';
    }
    $result .= '</a>';
    echo $result;
}
else{
    $args = array(
        'numberposts' => 1,
        'order'=> 'ASC',
        'post_mime_type' => 'image',
        'post_parent' =>  $post->ID,
        'post_status' => 'inherit',
        'post_type' => 'attachment',
        'orderby' => 'menu_order ID'
    );
    $attachments = get_children( $args );
    if ($attachments) {
        foreach($attachments as $attachment) {
//            $imgSRC = wp_get_attachment_image_src( $attachment->ID, 'thumbnail-blog' );
            //            $background = '<a href="'. get_the_permalink() .'"><div style="background-image:url('.$imgSRC[0].');" class="blog-single-thumbnail"/>';
            $image_id = get_post_thumbnail_id($attachment->ID);
            $image_attributes = wp_get_attachment_image_src($image_id, 'full');
            $img_meta       = wp_prepare_attachment_for_js($image_id);
            $image_title    = '';
            $image_alt      = $img_meta['alt'] == '' ? 'Falkon blog post attached image for post - '.esc_attr(get_the_title()) : $img_meta['alt'];

            $img_arg_array = array(
                "title" =>  $image_title,
                "alt" =>  $image_alt,
                "class" => "img-responsive blog-single-thumbnail-img"
            );
            $imgSRC = wp_get_attachment_image( $image_id, 'medium',"", $img_arg_array);
            $background = '<a href="'. get_the_permalink() .'" rel="nofollow" title="'.esc_attr(get_the_title()).'">'.$imgSRC;
        }
        $result = $background;
//        $result .= '</div></a>';
        $result .= '</a>';
        echo $result;
    }
    else{
        $result = '<a href="'. get_the_permalink() .'" rel="nofollow" title="'.esc_attr(get_the_title()).'"><img src="'.get_template_directory_uri().'/images/fallback-banner-768x352.jpg" class="blog-single-thumbnail-img" alt="default blog thumbnail image"/>';
        $result .= '</a>';
        echo $result;
    }
}
?>

