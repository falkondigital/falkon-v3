<?php global $falkon_option; ?>
<!-- MAIN MENU -->
<!-- Nav Modal -->
<div class="modal fade collapse" id="nav-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="container">
        <div class="row">
            <?php get_template_part( 'includes/blocks/nav-modal', 'content' ); ?>
        </div>
    </div>
</div>
<!--// Nav Modal -->
<div id="head-menu">
    <nav>
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/falkon-digital-logo.svg"
                         width="169px"
                         height="67px"
                         alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
                         onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/falkon-digital-logo.png';this.onerror=null;"
                         class="logo" />
                </a>
                <button id="menu-control" type="button" class="navbar-toggle collapsed" data-toggle="modal" data-target="#nav-modal" aria-expanded="false"  data-backdrop="static" keyboard="false">
                    <span class="menu-txt">Menu</span>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
            </div>
        </div>
    </nav>
</div>
<!-- // MAIN MENU -->
<!-- SCROLLING MENU -->
<?php /* ?>
<!-- Nav Modal -->
<div class="modal fade collapse" id="nav-modal22" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="container">
        <div class="row">
            <?php get_template_part( 'includes/blocks/nav-modal', 'content' ); ?>
        </div>
    </div>
</div>
<!--// Nav Modal -->
<?php */ ?>
<nav id="scroll-menu" class="scroller_default">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name','display' ).( get_bloginfo('description')!=''? ' - '.get_bloginfo('description'): '')); ?>" rel="nofollow home">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/white-nav-logo.svg"
                         width="50px"
                         height="30px"
                         alt="<?php echo get_bloginfo('name').' '.get_bloginfo('description'); ?>"
                         onerror="this.src='<?php echo get_template_directory_uri(); ?>/images/white-nav-logo.png';this.onerror=null;"
                         class="scroll_logo" />
                </a>
                <button id="menu-control" type="button" class="navbar-toggle navbar-scrolled-toggle collapsed" data-toggle="modal" data-target="#nav-modal" aria-expanded="false"  data-backdrop="static" keyboard="false">
                    <span class="menu-txt">Menu</span>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar top-bar"></span>
                    <span class="icon-bar middle-bar"></span>
                    <span class="icon-bar bottom-bar"></span>
                </button>
            </div>
        </div>
    </div>
</nav>
<!-- // SCROLLING MENU -->
