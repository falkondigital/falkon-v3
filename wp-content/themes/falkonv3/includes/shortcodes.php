<?php
/**
 *
 */

// our-work-shortcodes include
include_once 'blocks/our-staff-members.php';
include_once 'blocks/project-shortcodes.php';
include_once 'blocks/page-shortcodes.php';


add_shortcode("mic-get-trial-days","return_mic_get_trial_days");
function return_mic_get_trial_days($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $trial_in_days = $trial_weeks*7;
    $out = '<span class="trial-period trial-in-days">'.$trial_in_days.'</span>';
    return $out;
}

add_shortcode("mic-get-trial-weeks","return_mic_get_trial_weeks");
function return_mic_get_trial_weeks($atts, $content = null) {
    global $falkon_option;
    $trial_weeks = $falkon_option['falkon_trial_weeks'];
    $out = '<span class="trial-period trial-in-weeks">'.$trial_weeks.'</span>';
    return $out;
}

add_shortcode( 'mic-trial-button', 'build_mic_trial_button' );
function build_mic_trial_button($atts, $content = null) {
    extract(shortcode_atts(array(
        'xclass'	=> '',
    ), $atts));
    $xclass = ($xclass == '') ? '' : ' '.$xclass;
    global $falkon_option;
    $trial_register_id = (int)$falkon_option['falkon_register_school_page_id'];
    $out = '<a href="'.get_permalink($trial_register_id).'" class="btn'.$xclass.'" title="'.esc_attr(wp_strip_all_tags(do_shortcode($content))).'">'.do_shortcode($content).'</a>';
    return $out;
}


/**
 * @param $atts
 * @param null $content
 * @return string
 */
function falkon_themes_site_contact_email( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_email']!=NULL and $falkon_option['falkon_contact_email']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
        ), $atts));
        if($link) $out = '<a href="mailto:'.antispambot($falkon_option['falkon_contact_email'],1).'" title="'.antispambot($falkon_option['falkon_contact_email'],0).'" rel="nofollow">'.antispambot($falkon_option['falkon_contact_email'],0).'</a>';
        else $out = $falkon_option['falkon_contact_email'];
    }
    return $out;

}
add_shortcode('company-contact-email', 'falkon_themes_site_contact_email');

function falkon_themes_site_contact_phone( $atts, $content = null ) {
    $falkon_option = falkon_get_global_options();
    $out = '';
    if($falkon_option['falkon_contact_number']!=NULL and $falkon_option['falkon_contact_number']!=''){
        extract(shortcode_atts(array(
            'link'	=> false,
            'international'   =>  false,
        ), $atts));
        $number_is = $falkon_option['falkon_contact_number'];
        $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
        if($international){
            $number_is = substr($falkon_option['falkon_contact_number'], 1);
            $number_is = $number_is;
            $number_link = preg_replace('/\s+/', '', $falkon_option['falkon_contact_number']);
            $number_link = "044".substr($number_link, 1);
        }
        if($link) $out = '<a href="tel:'.antispambot($number_link,1).'" title="'.$number_is.'" rel="nofollow" class="telephone-link">'.$number_is.'</a>';
        else $out = $number_is;
    }
    return $out;
}
add_shortcode('company-contact-number', 'falkon_themes_site_contact_phone');

// Breakout of white bg to grey bg
function falkon_grey_breakout( $atts,  $content = null ) {
    ob_start();
    $a = shortcode_atts( [
         'content-return' =>'',

    ],
    $atts );

    //Breeakout out of page-content (white-bg)
    $output =  '</div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</section>';

    // BEGIN RESULTS BACKGROUND
    $output .= '<section id="grey-page-content">';
        $output .= '<div class="container">';
            $output .= '<div class="row text-center">';
                 $output .= wpautop(do_shortcode($content));
            $output .= '</div>';
        $output .= '</div>';
    $output .= '</section>';

// BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
        $output .= '<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">';
    }

    echo $output;
    return ob_get_clean();
}
add_shortcode( 'grey-bg', 'falkon_grey_breakout' );

// Full width banner
function falkoninnerbanner( $atts ) {
    ob_start();
    $a = shortcode_atts( [
        'image_id'=> false,
        'contentreturn' => '',
    ],
    $atts );

    // Get the image and the image's alt value.
    $image     = wp_get_attachment_image_src( $a['image_id'],'full' );

    // BREAKOUT
    $output = '</div></div></div></section>';

    // FULL WIDTH BANNER - no div because its not scaling
    $output .= '<img src="'.$image[0].'">';

    // BACK IN {-if set to yes-}
    if ( $a['contentreturn'] == 'Yes' || $a['contentreturn'] == 'yes' ) {
    $output .= '<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">';
    }

    echo $output;
    return ob_get_clean();
}
add_shortcode( 'falkon-full-banner', 'falkoninnerbanner' );




function falkon_client_carousel() {
    $block_location = 'carousel-clients';
    $carousel_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
    return $carousel_block;
}
add_shortcode('flkn_client_logos', 'falkon_client_carousel');


function falkon_legacy_content_block( $atts,  $content = null )
{
    return do_shortcode($content);
}
add_shortcode('content-block', 'falkon_legacy_content_block');
function falkon_legacy_page_socials( $atts,  $content = null )
{
    return do_shortcode($content);
}
add_shortcode('page-socials', 'falkon_legacy_page_socials');