<?php
/*
This loop calls title from custom page attributes (WPalchemy)
*/
//Custom title
global $custom_titles;
$custom_title = $custom_titles->the_meta();
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php
$page_title = $custom_title['custom-title']? $custom_title['custom-title']: get_the_title();
//var_dump($cst_titles);
   echo '<div class="h1-pt text-center">' . $page_title . '</div>';
   echo '<hr class="pink">';

?>
	<article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
		<?php the_content()?>
		<?php wp_link_pages(); ?>
	</article>
<?php endwhile; ?>
<?php else: get_template_part('includes/loops/content', 'none'); ?>
<?php endif; ?>
