<?php
/*
The Page Loop
=============
*/
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <div class="text-center">
    <?php
        //var_dump($cst_titles);
        if (isset($cst_titles['show_custom_title'])){
            echo '<div class="h1-pt">' . $cst_titles['custom-title'] . '</div>';
            echo '<hr class="pink">';
        }
        else  {
            echo the_title('<div class="h1-pt">', '</div>');
            echo '<hr class="pink">';
        }
    ?>
    </div>

        <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
            <?php the_content()?>
            <?php wp_link_pages(); ?>
        </article>

<?php endwhile; ?>
<?php else: get_template_part('includes/loops/content', 'none'); ?>
<?php endif; ?>
