<div class="post-<?php the_ID(); ?> post-holder">
    <?php
    $blog_trimtitle = wp_trim_words(get_the_title(),9,'...');
    $block_excerpt = wp_trim_words(get_the_excerpt(), 32, '...' );
    ?>
    <!-- Post Thumbnail -->
    <?php get_template_part('includes/blocks/blog-feed', 'thumbnail'); ?>
    <!-- Title & Content -->
     <div class="post-content">
         <div class="post-title">
            <a href="<?php the_permalink(); ?>" title="<?php esc_attr(get_the_title()); ?>">
                <h4><?php echo $blog_trimtitle; ?></h4>
            </a>
         </div>
         <div class="post-exrt">
            <p><?php echo $block_excerpt; ?></p>
         </div>
	 </div>
</div>
