<?php
/*
This loop calls title from custom page attributes (WPalchemy)
*/
//Custom title
global $custom_titles;
$cst_titles = $custom_titles->the_meta();
?>

<?php if(have_posts()): while(have_posts()): the_post(); ?>


	<article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
		<?php the_content()?>
		<?php wp_link_pages(); ?>
	</article>

<?php endwhile; ?>
<?php else: get_template_part('includes/loops/content', 'none'); ?>
<?php endif; ?>
