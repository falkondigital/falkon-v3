<?php
/*
The Blog  Loop (used by index.php)
=====================================================
*/
global $falkon_option;
$paged = (get_query_var('paged') ? get_query_var('paged') : 1);
// are we on page one?
$show_blog_title=true;
if(1 == $paged and is_home()) {
    echo '<section id="page-content">';
    echo '<div class="container">';

    // Latest Vlogs Section, uses a transient block
    if(isset($falkon_option['falkon_blog_vlogs_vis']) and $falkon_option['falkon_blog_vlogs_vis']=='1'){
        $block_location = 'vlogs-youtube-feed';
        $carousel_block = falkon_get_transient($block_location);
        echo $carousel_block;
    }

    // STICKY
    $sticky = get_option('sticky_posts');
    echo '<div class="row">';
    echo '<div class="h1-pt text-center">Featured Post</div>
                <hr class="pink">';
    // check if there are any
    if (!empty($sticky)) {
        // optional: sort the newest IDs first
        rsort($sticky);
        // override the query
        $args = array(
            'post__in' => $sticky,
            'posts_per_page' => 1,
        );
        query_posts($args);
        $stickypost = new WP_Query( $args );
        if ( $stickypost->have_posts() ) :
            while ( $stickypost->have_posts() ) : $stickypost->the_post();
                echo '<div class="col-xs-12 col-sm-offset-2 col-sm-8 text-center">';
                include(locate_template('includes/loops/post-sticky-content.php'));
                echo '</div>';
            endwhile;
            wp_reset_postdata();
        endif;
    }
    wp_reset_query();
    echo '</div>';
    ?>
    </div><!--breakout container-->
    </section><!--breakout section #page-content-->
<?php }

if(is_author() or is_archive()){
    $show_blog_title=false;
}
?>
<section id="grey-page-content">
    <div class="container">
        <!-- ALL POST -->
        <?php if($show_blog_title){ ?>
            <div class="row">
                <div class="h1-pt text-center">Blog</div>
                <hr class="pink">
            </div>
        <?php } ?>
        <?php if(!is_author()) include(locate_template('includes/blocks/blog-search-bar.php')); ?>
        <div class="row">
        <?php
        $wp_posts_per_page = get_option( 'posts_per_page' );
//        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
//        $args = array(
//            'posts_per_page' => $wp_posts_per_page,
//            'category_name ' => array('video-marketing'),
//            'ignore_sticky_posts' => $sticky,
//            'paged' => $paged,
//        );
//        $vidposts = new WP_Query( $args );
        if ( have_posts() ) :
            $xyz = 1;
            while ( have_posts() ) : the_post();
                if( is_sticky() and !is_archive()) {
                    continue;
                }
                else{
                    echo '<div class="col-xs-12 col-sm-4 text-center">';
                    include(locate_template('includes/loops/blog-feed-content.php'));
                    echo '</div>';
                    if ($xyz++ % 3 == 0) echo '<div class="clearfix visible-sm-block visible-md-block visible-lg-block "></div>';
                }
            endwhile;
            wp_reset_postdata();
        else: get_template_part('includes/loops/content', 'none');
        endif;
        wp_reset_query();
        ?>
        </div>
        <div class="row">
	        <div class="col-xs-12 col-sm-12" style="text-align: center;">
                <div class="pagination-flk">
                <?php if ( function_exists('bst_pagination') ) { bst_pagination(); } else if ( is_paged() ) { ?>
                      <ul class="pagination">
                        <li class="older"><?php next_posts_link('<i class="glyphicon glyphicon-arrow-left"></i> ' . __('Previous', 'bst')) ?></li>
                        <li class="newer"><?php previous_posts_link(__('Next', 'bst') . ' <i class="glyphicon glyphicon-arrow-right"></i>') ?></li>
                    </ul>
                <?php } ?>
		        </div>
	        </div>
        </div>
    </div>
</section>