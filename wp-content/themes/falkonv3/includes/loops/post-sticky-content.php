<div class="post-<?php the_ID(); ?> sticky-post-holder">
    <?php $block_excerpt = wp_trim_words(get_the_excerpt(), 75, '...' ); ?>
    <!-- Post Thumbnail -->
    <?php get_template_part('includes/blocks/blog-feed', 'thumbnail'); ?>
    <!-- Title & Content -->
     <div class="post-content">
         <div class="post-title">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                <h4><?php the_title(); ?></h4>
            </a>
         </div>
         <div class="post-exrt">
            <p><?php echo $block_excerpt; ?></p>
         </div>
	 </div>
</div>
