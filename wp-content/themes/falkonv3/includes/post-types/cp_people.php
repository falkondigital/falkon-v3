<?php
// Register Custom Post Type 'staff' (our-people)
function reg_post_testimonial() {
	$labels = array(
		'name'                => _x( 'Our Staff', 'Post Type General Name', 'falkondigital' ),
		'singular_name'       => _x( 'Our Staff', 'Post Type Singular Name', 'falkondigital' ),
		'menu_name'           => __( 'Our Staff', 'falkondigital' ),
		'parent_item_colon'   => __( 'Parent staff member', 'falkondigital' ),
		'all_items'           => __( 'View all staff', 'falkondigital' ),
		'view_item'           => __( 'View staff member', 'falkondigital' ),
		'add_new_item'        => __( 'Add new staff member', 'falkondigital' ),
		'add_new'             => __( 'New staff member', 'falkondigital' ),
		'edit_item'           => __( 'Edit staff member', 'falkondigital' ),
		'update_item'         => __( 'Update staff member', 'falkondigital' ),
		'search_items'        => __( 'Search staff', 'falkondigital' ),
		'not_found'           => __( 'No staff found', 'falkondigital' ),
		'not_found_in_trash'  => __( 'No staff found in Trash', 'falkondigital' ),
	);

	$args = array(
		'label'               => __( 'our-people', 'falkondigital' ),
		'description'         => __( 'People', 'falkondigital' ),
		'labels'              => $labels,
        'supports'            => array ('thumbnail','title','editor'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_position'       => 28,
		'menu_icon'             =>'dashicons-businessman',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => true,
		'publicly_queryable'  => false,
		'capability_type'     => 'page',
		'rewrite'            => array( 'slug' => 'our-people','with_front' => false ),
	);

	register_post_type( 'our-people', $args );
}

// Hook into the 'init' action
add_action( 'init', 'reg_post_testimonial', 0 );


/*
 * Add custom messages for the three custom post types
 */
function updated_messages_testimonial( $messages ) {
	global $post, $post_ID;
	$messages['our-people'] = array(
		0 => '',	// Unused. Messages start at index 1.
		1 => __('Our People updated.'),
		2 => __('Our People updated.'),
		3 => __('Our People deleted.'),
		4 => __('Person updated.'),
		5 => isset($_GET['revision']) ? sprintf( __('Testimonial restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => __('Person published.'),
		7 => __('Person saved.'),
		8 => sprintf( __('Person submitted. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('Person scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview car?</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('Person draft updated. <a target="_blank" href="%s">Preview car?</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'post_updated_messages', 'updated_messages_testimonial' );



function myactivationfunction_testimonial() {
	//my_custom_post_product();
 	flush_rewrite_rules();
}
add_action("after_switch_theme", "myactivationfunction_testimonial", 10 ,  2);
function mydeactivationfunction_testimonial() {
 	flush_rewrite_rules();
}
add_action("switch_theme", "mydeactivationfunction_testimonial", 10 ,  2);


//include_once get_template_directory() . '/metaboxes/wpalchemy/metabox.php';
//
//include_once get_template_directory() . '/metaboxes/ourpeople-spec.php';

//include_once get_template_directory() . '/metaboxes/testimonial-image-spec.php';



add_filter( 'manage_our-people_posts_columns', 'testimonial_columns_filter', 10, 1 );

function testimonial_columns_filter( $columns ) {

 	//$column_thumbnail = array( 'prop-thumb' => 'Image' );
//	$column_thumbnail = array( 'testimonial-thumb' => 'Thumbnail' );

	$column_propstatus = array( 'profile-image' => 'Staff Image' );
	$column_desc = array( 'profile-title' => 'Staff Name' );
	$column_featured = array( 'profile-desc' => 'Staff Description' );

	//$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
//	$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, NULL, true );
	$columns = array_slice( $columns, 0, 3, true ) + $column_desc + array_slice( $columns, 3, NULL, true );
	$columns = array_slice( $columns, 0, 4, true ) + $column_featured + array_slice( $columns, 4, NULL, true );

	$columns = array_slice( $columns, 0, 2, true ) + $column_propstatus + array_slice( $columns,2, NULL, true );

	//$columns = array_slice( $columns, 0, 8, true ) + $column_propfeat + array_slice( $columns, 8, NULL, true );
	unset($columns['title']);
	unset($columns['author']);
	unset($columns['date']);
	unset($columns['comments']);
	return $columns;

}

add_action( 'manage_our-people_posts_custom_column', 'my_column_action_testimonial', 10, 1 );

function my_column_action_testimonial( $column ) {
//var_dump("HELLo");
	global $post;


	switch ( $column ) {

        case 'profile-image':
            echo the_post_thumbnail('thumbnail');
        break;
		case 'profile-title':
            echo the_title('<h3>','</h3>');
		break;
        case 'profile-desc':
            echo wp_trim_words( get_the_content(),20,'...');
        break;

	}
}