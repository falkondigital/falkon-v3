<?php
// Register Custom Post Type 'case studies' (project)

function falkon_register_cp_our_work() {

    $labels = array(
        'name'                => _x( 'Our Case Studies', 'Post Type General Name', 'falkon' ),
        'singular_name'       => _x( 'Our Case Studies', 'Post Type Singular Name', 'falkon' ),
        'menu_name'           => __( 'Our Case Studies', 'falkon' ),
        'name_admin_bar'      => __( 'Our Case Studies', 'falkon' ),
        'parent_item_colon'   => __( 'Parent Our Case Studies:', 'falkon' ),
        'all_items'           => __( 'All Our Case Studies', 'falkon' ),
        'add_new_item'        => __( 'Add New Case Study', 'falkon' ),
        'add_new'             => __( 'Add New Case Study', 'falkon' ),
        'new_item'            => __( 'New Case Study', 'falkon' ),
        'edit_item'           => __( 'Edit Case Study', 'falkon' ),
        'update_item'         => __( 'Update Case Study', 'falkon' ),
        'view_item'           => __( 'View Case Study', 'falkon' ),
        'search_items'        => __( 'Search Our Case Studies', 'falkon' ),
        'not_found'           => __( 'No our work found', 'falkon' ),
        'not_found_in_trash'  => __( 'No our work found in Trash', 'falkon' ),
    );
    $args = array(
        'label'               => __( 'Our Case Studies', 'falkon' ),
        'description'         => __( 'Add a new awesome Falkon case study', 'falkon' ),
        'labels'              => $labels,
        'supports'            => array('title', 'author', 'thumbnail','editor'),
        'taxonomies'          => array( 'our-case-type','client' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-portfolio',
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => false,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'rewrite'            => array( 'slug' => 'case-studies','with_front' => false ),
        'capability_type'     => 'post',
//        'capability_type'     => 'our-work',//array('our-work','our-work'),
//        'capabilities'        => $capabilities,
//        'map_meta_cap' => true,
    );
    register_post_type( 'project', $args );
}
add_action( 'init', 'falkon_register_cp_our_work', 0 );


// Register Custom Taxonomy - our-work-type
function create_tax_our_work_our_work_type()  {
    $labels = array(
        'name'                       => _x( 'Case Study Features', 'Taxonomy General Name', 'falkon' ),
        'singular_name'              => _x( 'Case Study Features', 'Taxonomy Singular Name', 'falkon' ),
        'menu_name'                  => __( 'Case Study Features', 'prolixity' ),
        'all_items'                  => __( 'All Case Study Features', 'falkon' ),
        'parent_item'                => __( 'Parent Case Study Feature', 'falkon' ),
        'parent_item_colon'          => __( 'Parent Case Study Feature:', 'falkon' ),
        'new_item_name'              => __( 'New Case Study Feature', 'falkon' ),
        'add_new_item'               => __( 'Add Case Study Feature', 'falkon' ),
        'edit_item'                  => __( 'Edit Case Study Feature', 'falkon' ),
        'update_item'                => __( 'Update Case Study Feature', 'falkon' ),
        'separate_items_with_commas' => __( 'Separate Case Study Feature with commas', 'falkon' ),
        'search_items'               => __( 'Search Case Study Features', 'falkon' ),
        'add_or_remove_items'        => __( 'Add or remove Case Study Features', 'falkon' ),
        'choose_from_most_used'      => __( 'Choose from the most used Case Study Features', 'falkon' ),
    );
    $rewrite = array(
        'slug'                       => 'our-case-type',
        'with_front'                 => true,
        'hierarchical'               => false,
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => false,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'query_var'                 => true,
        'rewrite'                    => false,
    );
    register_taxonomy( 'our-case-type',array('project'), $args );

}
add_action( 'init', 'create_tax_our_work_our_work_type', 0 );


/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function falkon_save_meta_box_data_our_work( $data , $postarr ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
//    if ( ! isset( $_POST['myplugin_meta_box_nonce'] ) ) {
//        return;
//    }

    // Verify that the nonce is valid.
//    if ( ! wp_verify_nonce( $_POST['myplugin_meta_box_nonce'], 'myplugin_save_meta_box_data' ) ) {
//        return;
//    }

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
//    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
//
//        if ( ! current_user_can( 'edit_page', $post_id ) ) {
//            return;
//        }
//
//    } else {
//
//        if ( ! current_user_can( 'edit_post', $post_id ) ) {
//            return;
//        }
//    }

    /* OK, it's safe for us to save the data now. */
    remove_action( 'wp_insert_post_data', 'set_private_categories' );
    add_settings_error(
        'missing-death-star-plans',
        'missing-death-star-plans',
        'You have not specified the location for the Death Star plans.',
        'error'
    );
    set_transient( 'settings_errors', get_settings_errors(), 30 );
    add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
    return false;

    // Make sure that it is set.
    if ( ! isset( $_POST['myplugin_new_field'] ) ) {
        return;
    }

    // Sanitize user input.
    $my_data = sanitize_text_field( $_POST['myplugin_new_field'] );

    // Update the meta field in the database.
    update_post_meta( $post_id, '_my_meta_value_key', $my_data );
}
//add_action( 'wp_insert_post_data', 'myplugin_save_meta_box_data' );
//add_action( 'save_post', 'myplugin_save_meta_box_data' );





function change_default_title_our_work( $title ){
    $screen = get_current_screen();
    if  ( 'project' == $screen->post_type ) {
        $title = 'Enter project name here';
    }
    return $title;
}
add_filter( 'enter_title_here', 'change_default_title_our_work' );

/**
 * Setup meta boxes using WPAlchemy for backend management and filter/save use on frontend.
 */
//if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_school' );
function my_metabox_styles_our_work()
{
    wp_enqueue_style( 'wpalchemy-metabox-our_work', get_stylesheet_directory_uri() . '/metaboxes/meta-our_work.css');
}

$our_work_mb = new WPAlchemy_MetaBox(array
(
    'id' => '_our_work_meta',
    'title' => 'Our Case Studies Meta Info',
    'types' => array('project'),
    'context' => 'normal', // same as above, defaults to "normal"
    'priority' => 'high', // same as above, defaults to "high"
    'save_filter' => 'save_filter_our_work_data',
    'save_action' => 'save_action_our_work_meta',
    'template' => get_stylesheet_directory() . '/metaboxes/our-work-meta.php',
    'mode' => WPALCHEMY_MODE_EXTRACT,
    'prefix' => '_our_work_',
));

function save_filter_our_work_data($meta, $post_id){
//    var_dump('dfsdf');
//exit;
//    if ( 'our-work' == $_POST['post_type'] && !current_user_can( 'edit_our-work', $post_id )) {
//        return $post_id;
//    }

//    var_dump($_POST);
//    exit;
    if(isset($_POST['our-work_client'])) {
        $meta['client_name'] = $_POST['our-work_client'];
    }
    else
        unset($meta['client_name']);
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }

//    if(isset($meta['user_fname'])){
//        $meta['user_fname'] = 'bob';
//    }
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }
//    if(!isset($meta['address_country']) or $meta['address_country']==''){
//        add_settings_error(
//            'missing-our_work-meta-address_country',
//            'missing-our_work-meta-address_country',
//            'You must provide the our_work country.',
//            'error'
//        );
//    }
//    if(!isset($meta['client_id']) or $meta['client_id']==''){
//        add_settings_error(
//            'missing-our_work-meta-client',
//            'missing-our_work-meta-client',
//            'You must select a client from the drop down.',
//            'error'
//        );
//    }
//    var_dump($meta);
//    exit;
//    add_settings_error(
//        'missing-death-star-plans',
//        'missing-death-star-plans',
//        'You have not specified the location for the Death Star plans.',
//        'error'
//    );
//
//    add_settings_error(
//        'invalid-email',
//        '',
//        'You must define a value in the meta box.',
//        'error'
//    );

//    set_transient( 'settings_errors', get_settings_errors(), 30 );
//    return false;

//    var_dump($meta['equipment_required']);
//    exit;
    //Check for blank entries with qty = 1

//    var_dump($meta);


//exit;
//    if($meta['date_received']=='') unset($meta['date_received']);
//    if($meta['date_received']=='dd/mm/yyyy') unset($meta['date_received']);
//    if($meta['date_received']){
//        $meta['date_received'] = strtotime(str_replace('/','-',$meta['date_received']));
//    }

//    if($meta['date_required']=='') unset($meta['date_required']);
//    if($meta['date_required']=='dd/mm/yyyy') unset($meta['date_required']);
//    if($meta['date_required']){
//        $meta['date_required'] = strtotime(str_replace('/','-',$meta['date_required']));
//    }

//    if($meta['date_awarded']=='') unset($meta['date_awarded']);
//    if($meta['date_awarded']=='mm/yyyy') unset($meta['date_awarded']);
//    var_dump($meta['date_awarded']);
//    if($meta['date_awarded']){
//        $meta['date_awarded'] = strtotime("01-".str_replace('/','-',$meta['date_awarded']));
//    }
//    var_dump($meta['date_awarded']);
//exit;

//    if(isset($meta['equipment_required']) and is_array($meta['equipment_required'])){
//        foreach($meta['equipment_required'] as $key => $equipment){
//            if(count($equipment)==1 and isset($equipment['qty']))
//                unset($meta['equipment_required'][$key]);
//
//            if($equipment['equipment_id']=='' and $equipment['equipment_id_other']=='')
//                unset($meta['equipment_required'][$key]);
//            if($equipment['equipment_id'])
//            equipment_id_other
//        }
//    }
    //Re-index array
//    $meta['equipment_required'] = array_values($meta['equipment_required']);
//    var_dump($meta['equipment_required']);
//exit;

//    if(isset($meta['value']))
//        $meta['value'] = format_price($meta['value']);

//    var_dump($meta);
//exit;

    return $meta;
}


function save_action_our_work_meta($meta,$post_id){
    // Check permissions
    if ( 'our-work' == $_POST['post_type'] && !current_user_can( 'edit_posts', $post_id )) {
        return $post_id;
    }

    //Save Taxonomy to taxonomy metas
    //out-work-type
    $ourtaxonomy = $_POST['our-work_our-work-type'];
    wp_set_object_terms( $post_id, $ourtaxonomy, 'our-work-type' );

    //client
    $ourtaxonomy = $_POST['our-work_client'];
    wp_set_object_terms( $post_id, $ourtaxonomy, 'client' );
}



add_filter( 'manage_our_work_posts_columns', 'our_work_columns_filter', 10, 1 );
function our_work_columns_filter( $columns ) {
//    $column_status = array( 'our_work-status' => 'Status' );
//    $column_average_score = array( 'our_work-average-score' => 'Av Score' );
//    $column_average_time = array( 'our_work-average-time' => 'Av Time' );
//    $column_attempts_review = array( 'our_work-attempts-r' => 'Attempts Review' );
//    $column_attempts_practice = array( 'our_work-attempts-p' => 'Attempts Practice' );
//    $column_downloads = array( 'our_work-downloads' => 'Downloads' );

//    $columns = array_slice( $columns, 0, 3, true ) + $column_status + array_slice( $columns, 3, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_average_score + array_slice( $columns, 4, NULL, true );
//    $columns = array_slice( $columns, 0, 5, true ) + $column_average_time + array_slice( $columns, 5, NULL, true );
//    $columns = array_slice( $columns, 0, 6, true ) + $column_attempts_review + array_slice( $columns, 6, NULL, true );
//    $columns = array_slice( $columns, 0, 7, true ) + $column_attempts_practice + array_slice( $columns, 7, NULL, true );
//    $columns = array_slice( $columns, 0, 4, true ) + $column_downloads + array_slice( $columns, 4, NULL, true );

//    unset($columns['title']);
//    unset($columns['author']);
    unset($columns['date']);
    unset($columns['comments']);
    return $columns;
}
add_action( 'manage_our_work_posts_custom_column', 'our_work_column_action', 10, 1 );
function our_work_column_action( $column ) {
    global $post;
    switch ( $column ) {

    }
}


add_action('pre_get_posts','db_query_alter_query_our_work');
function db_query_alter_query_our_work($query){

    global $post;
    global $falkon_option;

    if (!is_admin()
        and !$query->is_main_query()
        and $post->ID == (int)$falkon_option['falkon_our_work_page_id']
        and $query->get('post_type')=='our-work') {

        //Do something to main query
        //echo '<pre>'; var_dump($_GET); echo '</pre>'; die;
        //echo '<pre>'; var_dump($query); echo '</pre>'; die;

        $orderby = 'menu_order title';//"&orderby=cost&order=DESC";
        $order = "ASC";

        //Set order by if set in URL
        if (isset($_GET['orderby'])) {
            switch ($_GET['orderby']) {
                case 'client':
                    $orderby = 'meta_value';//"&orderby=cost&order=DESC";
                    $order = "ASC";
                    $metaorder = "_our_work_client_name";
                    //$query->set('meta_key',$metaorder );
                    break;
            }
        }
        $query->set('meta_key', $metaorder);

        //Create the exclusion rule
        $new_tax_query = '';

        if(isset($_GET['work-type']) and $_GET['work-type']!=''){
            $makes = explode(",", $_GET['work-type']);
            $new_tax_query = array(
                'taxonomy' => 'our-work-type',
                'field'    => 'slug',
                'terms'    => $makes,
            );
        }


        //If there is already a tax_query, 'AND' our new rule with the entire existing rule.
        $tax_query = $query->get('tax_query');
        if($tax_query=='') $tax_query = array();
        if(!empty($tax_query)) {
            $new_tax_query = array(
                'relation' => 'AND',
                $tax_query,
                $new_tax_query,
            );
        }
        else{
            $new_tax_query = array(
                'relation' => 'AND',
                $new_tax_query
            );
        }
        $query->set('tax_query',$new_tax_query);

        //If there is already a meta_query, 'AND' our new rule with the entire existing rule.
        $meta_query = $query->get('meta_query');
        //var_dump($meta_query);
        if(!empty($meta_query)) {
            $new_meta_query = array(
                'relation' => 'AND',
                $meta_query,
                $new_meta_query,
            );
        }
        else{
            $new_meta_query = array(
                'relation' => 'AND',
                $new_meta_query,
            );
        }
        //var_dump($meta_query);
        $query->set('meta_query', $new_meta_query);

        //Finally set the order and orderby from the above
        $query->set('orderby',$orderby );
        $query->set('order',$order );
//        var_dump($query);
    }
}