<?php
/*
All the functions are in the PHP pages in the functions/ folder.
*/

define('FALKON_USE_TRANSIENTS', true);
define('FALKON_SHOW_TIMERS', false);
define('FALKON_SHOW_SITE_TIMER', false);

require_once locate_template('/functions/cleanup.php');
require_once locate_template('/functions/setup.php');
require_once locate_template('/functions/enqueues.php');
require_once locate_template('/functions/navbar.php');
require_once locate_template('/functions/widgets.php');
require_once locate_template('/functions/search.php');
require_once locate_template('/functions/feedback.php');

require_once locate_template('/functions/functions-emails.php');

add_action('after_setup_theme', 'true_load_theme_textdomain');

// Register WP alchemy
include_once 'metaboxes/setup.php';

// Custom Meta


include_once 'metaboxes/titles_spec.php';
include_once 'metaboxes/norot_banner-spec.php';

//include_once 'metaboxes/gallery-spec.php';
//include_once 'metaboxes/footer_spec.php';

//Load Cron Job functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-cron.php');

require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_people.php');
require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_our-work.php');

//Load common shared Falkon functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-falkon.php');
//Load common shared Falkon Hook functions
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-hooks.php');
//Load common functions to work with
require_once( trailingslashit(get_stylesheet_directory()).'functions/functions-transients-cache.php');

require_once( trailingslashit(get_stylesheet_directory()).'includes/shortcodes.php');

//require_once( trailingslashit(get_stylesheet_directory()).'includes/blocks/carousel-block.php');
//require_once( trailingslashit(get_stylesheet_directory()).'inc/widget.php');


function true_load_theme_textdomain(){
    load_theme_textdomain( 'bst', get_template_directory() . '/languages' );
}


// Blog searches - remove pages



// ignore sticky from getting all posts
//function main_query_ignore_sticky($query)
//{
//    if ($query->is_main_query())
//        $query->set('ignore_sticky_posts', true);
//    $query->set('post__not_in', get_option('sticky_posts'));
//}
//add_action('pre_get_posts', 'main_query_ignore_sticky');

//
////Load Page Custom Post type
//require_once( trailingslashit(get_stylesheet_directory()).'includes/post-types/cp_our-work.php');
//