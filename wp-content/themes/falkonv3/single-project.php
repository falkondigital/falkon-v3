<?php
/**
 * Single post template for CPT 'project'
 */
?>
<?php
	// Falkon Options
	global $falkon_option;
?>
<?php get_header();


?>
		<section id="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12">
			              <?php get_template_part('includes/loops/content', 'project'); ?>
                    </div>
                </div>
            </div>
		</section>

<?php

        include(locate_template('includes/blocks/related-work.php'));

get_footer();
?>
