<?php
get_header();
// Falkon Options
global $falkon_option;
?>

<section id="page-content">
    <div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
               <?php get_template_part('includes/loops/content-custom-title'); ?>
            </div>
		</div>
	</div>
</section>
<?php /* ?><div id="cta-first-home-wrapper" class="bg-blue text-white">
	<div class="container">
		<div class="row py-8 vertical-align">
			<div class="col-md-6 text-right align-self-center mega-image">
				<img src="https://www.scriptworks.io/wp-content/themes/scriptworks/images/tmp/macbook-features.png">
			</div>
			<div class="col-md-6 text-center align-self-center">
				<p class="h3">A Test Automation Platform</p>
				<p>Scriptworks is a subscription based automated testing offering targeted at making easy script creation for Mobile, Web and API testing built around Selenium and Appium. </p>
				<a href="https://www.scriptworks.io/pricing/" title="Pricing" class="btn btn-outline-white btn-lg">View Subscription Pricing</a>
			</div>

		</div>
	</div>
</div><?php */ ?>
<?php get_footer(); ?>
