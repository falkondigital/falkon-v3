<?php
/**
 *
 */

//wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS ".date("l jS \of F Y h:i:s A",time()),'');

function add_falkon_schedules( $schedules ) {

    $schedules['fifteenminutes'] = array(
        'interval' => 15* MINUTE_IN_SECONDS,
        'display' => __('Every 15 Minutes')
    );
    $schedules['thirtyminutes'] = array(
        'interval' => 30* MINUTE_IN_SECONDS,
        'display' => __('Every 30 Minutes')
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'add_falkon_schedules' );

//add_action("init", "mic_enable_cron_jobs");   //Used in testing to enable all cron jobs below.
add_action("after_switch_theme", "mic_enable_cron_jobs");
function mic_enable_cron_jobs(){
    _log("CRON - ENABLE CRONS");

    //Use wp_next_scheduled to check if the event is already scheduled
    $timestamp = wp_next_scheduled( 'falkon_check_instagram_images' );
    //If $timestamp == false schedule daily task since it hasn't been done previously
    if( $timestamp == false ){
        //Schedule the event for right now, then to repeat daily using the hook 'falkon_check_instagram_images'
        wp_schedule_event( strtotime('1982-03-12 00:00:00 UTC'), 'fifteenminutes', 'falkon_check_instagram_images' );
    }
//
//    //Use wp_next_scheduled to check if the event is already scheduled
//    $timestamp = wp_next_scheduled( 'mic_schools_trial_expired_reminder' );
//    //If $timestamp == false schedule daily task since it hasn't been done previously
//    if( $timestamp == false ){
//        //Schedule the event for right now, then to repeat daily using the hook 'wi_create_daily_backup'
//        wp_schedule_event( strtotime('2015-03-12 00:10:00 UTC'), 'daily', 'mic_schools_trial_expired_reminder' );
//    }
//
//    $timestamp = wp_next_scheduled( 'mic_check_schools_subscriptions' );
//    if( $timestamp == false ){
//        wp_schedule_event( strtotime('2015-03-12 00:15:00 UTC'), 'daily', 'mic_check_schools_subscriptions' );
//    }
//
//    $timestamp = wp_next_scheduled( 'mic_check_invoices_unpaid' );
//    if( $timestamp == false ){
//        wp_schedule_event( strtotime('2015-03-12 00:30:00 UTC'), 'daily', 'mic_check_invoices_unpaid' );
//    }
}

/**
 * Delete custom crons on theme deactivate.
 */
function mic_disable_cron_jobs(){
    _log("DISABLE CRONS");

    //Remove the dealer nag scheduled task
    wp_clear_scheduled_hook( 'falkon_check_instagram_images' );
}
add_action('switch_theme', 'mic_disable_cron_jobs');



//Hook our function , wi_create_backup(), into the action wi_create_daily_backup
add_action( 'falkon_check_instagram_images', 'falkon_check_instagram_images_cron_function' );
function falkon_check_instagram_images_cron_function(){
    //Run code to delete db entries.
    _log("CRON - DOING ACTION falkon_check_instagram_images_cron_function ".date("l jS \of F Y h:i:s A",time()));
    wp_mail('wordpress@falkondigital.com',"CRON - DOING CRONS action falkon_check_instagram_images_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON - DOING CRONS action falkon_check_instagram_images_cron_function ".date("l jS \of F Y h:i:s A",time()));

    do_action("falkon_get_instagram_images_and_save");
}

//Hook our function , wi_create_backup(), into the action wi_create_daily_backup
add_action( 'mic_check_schools_trial', 'mic_check_schools_trial_cron_function' );
function mic_check_schools_trial_cron_function(){
    //Run code to delete db entries.
    _log("CRON TEST - DOING ACTION mic_check_schools_trial_cron_function ".date("l jS \of F Y h:i:s A",time()));
    wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS action mic_check_schools_trial_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON TEST - DOING CRONS action mic_check_schools_trial_cron_function ".date("l jS \of F Y h:i:s A",time()));

    do_action("mic_update_school_trials_to_expired");
}

add_action( 'mic_schools_trial_expired_reminder', 'mic_schools_trial_expired_reminder_cron_function' );
function mic_schools_trial_expired_reminder_cron_function(){
    //Run code to delete db entries.
    _log("CRON TEST - DOING ACTION mic_schools_trial_expired_reminder_cron_function ".date("l jS \of F Y h:i:s A",time()));
    wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS action mic_schools_trial_expired_reminder_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON TEST - DOING CRONS mic_schools_trial_expired_reminder_cron_function mic_check_schools_trial_cron_function ".date("l jS \of F Y h:i:s A",time()));

    do_action("mic_schools_trial_expired_reminder_action");
}


//Hook our function , wi_create_backup(), into the action wi_create_daily_backup
add_action( 'mic_check_schools_subscriptions', 'mic_check_schools_subscriptions_cron_function' );
function mic_check_schools_subscriptions_cron_function(){
    //Run code to delete db entries.
    _log("CRON TEST - DOING ACTION mic_check_schools_subscriptions_cron_function ".date("l jS \of F Y h:i:s A",time()));
    wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS action mic_check_schools_subscriptions_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON TEST - DOING CRONS action mic_check_schools_subscriptions_cron_function ".date("l jS \of F Y h:i:s A",time()));

    do_action("mic_check_schools_subscriptions_action");
}


add_action( 'mic_check_invoices_unpaid', 'mic_check_invoices_unpaid_cron_function' );
function mic_check_invoices_unpaid_cron_function(){
    //Run code to delete db entries.
    _log("CRON TEST - DOING ACTION mic_check_invoices_unpaid_cron_function ".date("l jS \of F Y h:i:s A",time()));
    wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS action mic_check_invoices_unpaid_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON TEST - DOING CRONS action mic_check_invoices_unpaid_cron_function ".date("l jS \of F Y h:i:s A",time()));

    do_action("mic_check_invoices_unpaid_action");
}



//$month_in_seconds = WEEK_IN_SECONDS * 4;






//Hook our function , wi_create_backup(), into the action wi_create_daily_backup
//add_action( 'mic_test_cron_function', 'mic_test_cron_function' );
function mic_test_cron_function2(){
    //Run code to delete db entries.
    _log("CRON TEST - DOING ACTION mic_delete_user_check_db_function".date("l jS \of F Y h:i:s A",time()));
    wp_mail('steve@seo-creative.co.uk',"CRON TEST - DOING CRONS action mic_test_cron_function ".date("l jS \of F Y h:i:s A",time()),"CRON TEST - DOING CRONS action mic_test_cron_function ".date("l jS \of F Y h:i:s A",time()));
}