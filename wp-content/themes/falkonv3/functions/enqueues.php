<?php

function bst_enqueues() {

	/* Styles */

//	wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.4', null);
//	wp_enqueue_style('bootstrap-css');

//  	wp_register_style('bst-css', get_template_directory_uri() . '/css/bst.css', false, null);
//	wp_enqueue_style('bst-css');



    // style css with time stamp
    wp_enqueue_style( 'theme-css', get_template_directory_uri().'/css/style.css',array(),filemtime( get_template_directory() . '/css/style.css') );

	// Animate CSS for transitions
	wp_register_style('animate-css', get_template_directory_uri() . '/css/components/animate.css', false, null);
//	wp_enqueue_style('animate-css');

//	wp_register_style('owl-css', get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.css', false, null);
//	wp_enqueue_style('owl-css');

//	wp_register_style('owl-transitions', get_template_directory_uri() . '/includes/owl-carousel/owl.transitions.css', false, null);
//	wp_enqueue_style('owl-transitions');

//	wp_register_style('owl-theme', get_template_directory_uri() . '/includes/owl-carousel/owl.theme.css', false, null);
//	wp_enqueue_style('owl-theme');

	//Include theme support for Font Awesome icon pack
//	wp_enqueue_style( 'font-awesome',"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",array('theme-css'),'4.7.0');
	wp_register_style( 'font-awesome',"//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",array(''),'4.7.0');

	//Include theme support for all Open Sans font from Google APIS
	$query_args = array(
        'family' => 'Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,400,400i,700|Montserrat:300,400,400i,500,500i,700,800|Cantata+One:300,400,400i,700,800',
		'subset'	=>	'latin-ext',
	);
//	wp_enqueue_style( 'google_fonts_all', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );
	wp_register_style( 'google_fonts_all', add_query_arg( $query_args, "//fonts.googleapis.com/css" ), array(), null );


	/* Scripts */

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script('js-theme-apps',get_stylesheet_directory_uri().'/js/apps.js', array('jquery'), filemtime( get_stylesheet_directory().'/js/apps.js'), true);
	//Set Admin Ajax URL
	wp_localize_script( 'js-theme-apps', 'the_ajax_script_app', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
	/* Note: this above uses WordPress's onboard jQuery. You can enqueue other pre-registered scripts from WordPress too. See:
	https://developer.wordpress.org/reference/functions/wp_enqueue_script/#Default_Scripts_Included_and_Registered_by_WordPress */

  	wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr-2.8.3.min.js', false, null, true);
	wp_enqueue_script('modernizr');

  	wp_register_script('bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', false, null, true);
	wp_enqueue_script('bootstrap-js');

	wp_register_script('bst-js', get_template_directory_uri() . '/js/bst.js', false, null, true);
	wp_enqueue_script('bst-js');

//	wp_register_script('owl-js', get_template_directory_uri() . '/includes/owl-carousel/owl.carousel.js', false, null, true);
//	wp_enqueue_script('owl-js');


	wp_enqueue_script('jquery-owl',get_template_directory_uri().'/plugins/owlcarousel2/owl.carousel.js', array('jquery'), '2.3.4', true );

	wp_register_style('owl-css', get_template_directory_uri() . '/plugins/owlcarousel2/assets/owl.carousel.css', false, '2.3.4', null);
//	wp_enqueue_style('owl-css');
	wp_register_style('owl-css-default-theme', get_template_directory_uri() . '/plugins/owlcarousel2/assets/owl.theme.default.min.css', false, '2.3.4', null);
//	wp_enqueue_style('owl-css-default-theme');




	//Include Retina JS to replace embedded images with higher ...@2x images for HD displays.
	wp_enqueue_script('jquery-retina',get_template_directory_uri().'/js/retina.js', array('jquery'), '1', true );


	wp_register_script('youtube-background', get_template_directory_uri() . '/js/jquery.youtubebackground.js', false, null, true);
	wp_enqueue_script('youtube-background');
	wp_register_style('youtube-background-css', get_template_directory_uri() . '/css/ytbgstyle.css', false, null);
	wp_enqueue_style('youtube-background-css');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'bst_enqueues', 100);


function wp_67472455() {
	wp_dequeue_style( 'contact-form-7' );
	wp_dequeue_style( 'cookie-notice-front' );

}
add_action( 'wp_print_styles', 'wp_67472455', 100 );

add_filter('print_late_styles','load_any_style');
if(!function_exists('load_any_style')) {
	function load_any_style($print) {
		global $wp_styles;

		$handle_check = array('font-awesome','google_fonts_all','animate-css','owl-css','owl-css-default-theme', 'contact-form-7','cookie-notice-front' );
		foreach($handle_check as $css_handle){
			if(!in_array($css_handle,$wp_styles->done)){
				$enqueue_array = $wp_styles->registered;
				if(array_key_exists($css_handle,$enqueue_array)){
					$enqueue_css = $enqueue_array[$css_handle];
					$src = add_query_arg('ver',$enqueue_css->ver,$enqueue_css->src);
					echo "<link rel='stylesheet' id='$enqueue_css->handle'  href='$src' type='text/css' media='all' />\n";
				}
			}
		}
	}
}
