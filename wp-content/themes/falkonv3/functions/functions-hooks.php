<?php
/**
 *
 */

add_action('falkon_hook_before_wp_head', 'falkon_hook_before_wp_head_callback');
function falkon_hook_before_wp_head_callback(){
    global $falkon_option;

    if(isset($falkon_option['falkon_hook_before_wp_head']) and $falkon_option['falkon_hook_before_wp_head']!=''){
//        _log($falkon_option['falkon_hook_before_wp_head']);
        echo stripslashes($falkon_option['falkon_hook_before_wp_head']);
        echo "\n";
    }
}
add_action('falkon_hook_after_body', 'falkon_hook_after_body_callback');
function falkon_hook_after_body_callback(){
    global $falkon_option;

    if(isset($falkon_option['falkon_hook_after_body']) and $falkon_option['falkon_hook_after_body']!=''){
//        _log($falkon_option['falkon_hook_after_body']);
        echo stripslashes($falkon_option['falkon_hook_after_body']);
        echo "\n";
    }
}