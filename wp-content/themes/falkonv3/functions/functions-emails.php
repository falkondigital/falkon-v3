<?php
/**
 *
 */

add_action( 'phpmailer_init', 'configure_smtp' );
function configure_smtp( PHPMailer $phpmailer ){
    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);

    if($falkon_option['falkon_email_smtp_enable']=='1'){
//        var_dump($falkon_option['falkon_email_smtp_enable']);
//        exit;
        $phpmailer->isSMTP(); //switch to smtp
        $phpmailer->Host = $falkon_option['falkon_email_smtp_host'];
        $phpmailer->SMTPAuth = $falkon_option['falkon_email_smtp_auth'];
        $phpmailer->Port = $falkon_option['falkon_email_smtp_port'];
        $phpmailer->Username = $falkon_option['falkon_email_smtp_user']?$falkon_option['falkon_email_smtp_user']:'';
        $phpmailer->Password = $falkon_option['falkon_email_smtp_pass']?$falkon_option['falkon_email_smtp_pass']:'';
        $phpmailer->SMTPSecure = $falkon_option['falkon_email_smtp_secure'];
        $phpmailer->From = $falkon_option['falkon_email_from_address'];
        $phpmailer->FromName=$falkon_option['falkon_email_from_name'];
    }
}

/**
 * Basic function to return html content type for emails
 *
 * @return string
 */
function set_html_content_type() {
    return 'text/html';
}



/** changing default wordpres email settings */

add_filter('wp_mail_from', 'new_mail_from');
add_filter('wp_mail_from_name', 'new_mail_from_name');

function new_mail_from($old) {
    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
    $email_company_address = ($falkon_option['falkon_email_from_address']!=''?$falkon_option['falkon_email_from_address']:$old);
    return $email_company_address;
}
function new_mail_from_name($old) {
    if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
    $email_company_name = ($falkon_option['falkon_email_from_name']!=''?$falkon_option['falkon_email_from_name']:get_bloginfo('name'));
    return $email_company_name;
}

