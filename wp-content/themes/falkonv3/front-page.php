<?php
/**
 * Front page of the website, the big deal!
 */
?>
<?php
// Falkon Options
global $falkon_option;

get_header();

get_template_part( 'includes/blocks/our-work', 'thumbs');
?>
<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 text-center">
                <?php get_template_part('includes/loops/content-custom-title'); ?>
            </div>
        </div>
    </div>
</section>
<section id="page-content">
	<div class="container">
        <div class="row text-center">
            <div class="h1-pt"><a href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>" rel="nofollow">Latest News</a></div>
            <hr class="pink">
            <div class="col-xs-12 col-sm-12 col-md-8">
                <?php
                    $sticky = get_option('sticky_posts');
                    // check if there are any
                    if (!empty($sticky)) {
                        // optional: sort the newest IDs first
                        rsort($sticky);
                        // override the query
                        $args = array(
                            'post__in' => $sticky,
                            'posts_per_page' => 1,
                        );
                        query_posts($args);
                        $stickypost = new WP_Query( $args );
                        if ( $stickypost->have_posts() ) :
                            while ( $stickypost->have_posts() ) : $stickypost->the_post();
                                include(locate_template('includes/loops/post-home-sticky-content.php'));
                            endwhile;
                            wp_reset_postdata();
                        endif;
                    }
                    wp_reset_query();
                ?>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4">
				<?php
                    $args = array(
                        'orderby' => 'post_date',
                        'order' => 'DESC',
                        'post_type' => 'post',
                        'post_status' => 'publish',
                        'posts_per_page' => 2,
                        'ignore_sticky_posts'	=> 1,
                        'post__not_in' => get_option( 'sticky_posts' ),
                    );
                    $recent_posts = new WP_Query( $args );
                    if ( $recent_posts->have_posts() ) :
                            while ( $recent_posts->have_posts() ) : $recent_posts->the_post();
                                include(locate_template('includes/loops/blog-feed-content.php'));
                            endwhile;
                        wp_reset_postdata();
                    endif;
                    wp_reset_query();
                ?>
            </div>
<!--                <a href="--><?php //echo esc_url( home_url( '/blog' ) ); ?><!--" class="btn button ghost-btn-drk">View the blog</a>-->
		</div>
	</div>
</section>
<?php
// Insta Section, uses a transient block
if(isset($falkon_option['falkon_homepage_instagram_vis']) and $falkon_option['falkon_homepage_instagram_vis']=='1'){
    $block_location = 'instagram-feed';
    $carousel_block = falkon_get_transient($block_location);//get_transient( 'block-' . $block_location );
    echo $carousel_block;
}
?>
<?php get_footer(); ?>