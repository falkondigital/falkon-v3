<?php get_header();?>


<section id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div id="content" role="main">
                    <?php get_template_part('includes/loops/content', 'single'); ?>
                </div><!-- /#content -->
            </div>
        </div><!-- /.row -->
    </div><!-- /.container -->
</section>
<!-- Related Content -->
<div class="grey-bg">
<?php get_template_part('includes/blocks/related-category', 'posts'); ?>
</div>
<?php get_footer();?>


