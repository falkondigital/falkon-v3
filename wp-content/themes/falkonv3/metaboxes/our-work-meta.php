<div class="my_meta_control">
    <?php //var_dump($mb->meta);?>
    <table class="form-table">
        <tbody>

<tr>
    <?php $mb->the_field('hover_tagline'); ?>
    <th scope="row"><label for="<?php $mb->the_name(); ?>">Hover Sub Title:</label></th>
    <td>
        <input type="text" class="widefat" name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="title_line" maxlength="150" />
        <p class="description">Sub title of the hover state of each work entry</p>
    </td>
</tr>


<tr>
    <?php $mb->the_field('related_items'); ?>
    <?php
    $custom_post = 'project';
    $custom_post_object = get_post_type_object( $custom_post );
    $custom_post_name = $custom_post_object->labels->name;
    ?>
    <th scope="row">
        <label for=""><?php _e('Related '.$custom_post_object->labels->name,'falkon');?>:</label>
        <a href="#" id="checkall-<?php echo $custom_post;?>" title="Check or uncheck all <?php echo $custom_post_name;?>">Check/Uncheck All</a>
        <span><em>Select some work to appear in the related area</em></span>
        <script type="text/javascript">
            jQuery( document ).ready(function($) {
                $("#checkall-<?php echo $custom_post;?>").click(function() {
                    var checkBoxes = $('#autochecklist-<?php echo $custom_post;?> .autocheck');
                    checkBoxes.prop("checked", !checkBoxes.prop("checked"));
                    return false;
                });
                return false;
            });
        </script>
    </th>
    <td>
        <?php
        $original_post = $post;
        $args = array(
            'post_type' => $custom_post,
            'posts_per_page' => -1,
            'post__not_in' => array( get_the_ID()),
            'orderby'   => 'menu_order title'
        );
        $item_posts = get_posts( $args );

        if ( count($item_posts)>0 ) :
            echo '<div id="autochecklist-'.$custom_post.'"><ul class="">';
            foreach( $item_posts as $post_rel ) :
                setup_postdata($post_rel);
                echo sprintf('<li><label for="%s">', $post_rel->ID );
                echo sprintf('<input class="autocheck" type="checkbox" name="%s[]" value="%s" id="%2$s" %s/> %s</label></li>', $mb->get_the_name(), $post_rel->ID, $mb->get_the_checkbox_state($post_rel->ID),get_the_title($post_rel->ID) );
            endforeach;
            echo '</ul></div>';
        else:
            echo '<p>'.$custom_post_object->labels->not_found.'. <a href="'.admin_url('post-new.php?post_type='.$custom_post).'" target="_blank">Add one?</a></p>';
        endif;
        setup_postdata( $original_post);
        ?>
    </td>

</tr>


        </tbody>
    </table>
</div>