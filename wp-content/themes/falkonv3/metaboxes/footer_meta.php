<?php
// Falkon Options
global $falkon_option;
global $footercta_meta; ?>

<div class="my_meta_control">
    <!-- START tickbox meta-->
	<label for="<?php $mb->the_name(); ?>">Hide Pre Footer Blue CTA</label>
		<?php $mb->the_field('show_footer_cta'); ?>

	<p class="description"><input type="checkbox" id="show_footer_cta" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Tick this box if you want to hide the blue pre footer (towards the bottom of the page.) Pre footer reads 'Feel better now. Call us on <?php echo $falkon_option['falkon_contact_number'] ?>'</p>
	<!--END tickbox meta-->




</div>

