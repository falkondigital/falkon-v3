<!--<div class="banner_meta_control">-->
<!---->
<!--    <p><strong>Please Note : If the following fields contain no value, a default image and title will be displayed. </strong></p>-->
<!---->
<!--        --><?php //global $wpalchemy_media_access; ?>
<!---->
<!---->
<!--	                <p>-->
<!--		                Upload an image for the banner of this page, suggested image size: 1900px by 750px:-->
<!--	                </p>-->
<!--		                --><?php //$wpalchemy_media_access->setGroupName('img-n'. $mb->get_the_index())->setInsertButtonLabel('Insert Image'); ?>
<!--                    <p>-->
<!--                        --><?php //$mb->the_field('imgurl'); ?>
<!--                        --><?php //echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-url')); ?>
<!--                        --><?php //$mb->the_field('image_id'); ?>
<!--                        --><?php //echo $wpalchemy_media_access->getField(array('type'=>'hidden','name' => $mb->get_the_name(), 'value' => $mb->get_the_value(),'class'=>'upload-id')); ?>
<!--                        --><?php //echo $wpalchemy_media_access->getButton(array('label' => 'Upload Image')); ?>
<!--                        --><?php //$mb->the_field('imgurl'); ?>
<!--                        --><?php
//                        ?>
<!--                        <img src="--><?php //echo (is_null($mb->get_the_value())?'':$mb->get_the_value());?><!--" class="slideimg default-image">-->
<!--                        <a title="Delete Image" class="flkn-delete-banner submitdelete alignright" href="#" >Delete Banner</a>-->
<!--                    </p>-->
<!---->
<!---->
<!--	--><?php //$mb->the_field('title_line'); ?>
<!--	<label for="--><?php //$mb->the_name(); ?><!--">Banner title</label>-->
<!--	<p>-->
<!--		<input type="text" style="width:100%;" id="--><?php //$mb->the_name(); ?><!--" name="--><?php //$mb->the_name(); ?><!--" value="--><?php //$mb->the_value(); ?><!--" />-->
<!--	</p>-->
<!---->
<!--    --><?php //$mb->the_field('ban_tag'); ?>
<!--    --><?php
//    $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
//    $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
//    $settings = array(
//        'quicktags' => array(
//            'buttons' => 'p,em,strong,link,ul,ol',
//        ),
//        /*'quicktags' => true,*/
//        'tinymce' => true,
//        'media_buttons'	=> false,
//        'textarea_name'	=> $mb->get_the_name(),
//        'textarea_rows'	=> 14,
//        'teeny'			=> true,
//    );
//
//    wp_editor($content, $id, $settings);
//
//    ?>
<!---->
<!---->
<!--</div>-->

<table class="form-table">
    <tbody>
    <tr>
        <?php $mb->the_field('title_line'); ?>
        <th scope="row"><label for="<?php $mb->the_name(); ?>">Banner Title:</label></th>
        <td>
            <input type="text"
                   class="widefat"
                   name="<?php $mb->the_name(); ?>" value="<?php $mb->the_value(); ?>" id="title_line"
                   maxlength="150" />
            <p class="description">Title line of the banner.</p>
        </td>
    </tr>
    <tr>
        <?php $mb->the_field('banner_tag'); ?>
        <th scope="row"><label for="<?php $mb->the_name(); ?>">Banner Tag Text:</label></th>
        <td>
            <?php $mb->the_field('ban_tag'); ?>
            <?php
            $content = html_entity_decode($mb->get_the_value(),ENT_QUOTES, 'UTF-8');
            $id = sanitize_key($mb->get_the_name());//Needs to not have [] in the id, only letters and underscores
            $settings = array(
                'quicktags' => array(
                    'buttons' => 'p,em,strong,link,ul,ol',
                ),
                /*'quicktags' => true,*/
                'tinymce' => true,
                'media_buttons'	=> false,
                'textarea_name'	=> $mb->get_the_name(),
                'textarea_rows'	=> 14,
                'teeny'			=> true,
            );

            wp_editor($content, $id, $settings);

            ?>
        </td>
    </tr>
    <tr>
        <?php $mb->the_field('image_id'); ?>
        <?php $max_image_num = 1;?>
        <th scope="row"><label for="<?php $mb->the_id(); ?>">Upload Banner Image:</label></th>
        <td id="banner-upload-td">
            <input id="upload_banner_button" type="button" class="button alignleft" value="Upload Image" />
            <a title="Delete Image" class="flkn-delete-banner submitdelete alignright" href="#" >Delete Banner</a>
            <br><br><p class="description">Upload a custom image for use in the banner.
                If you do not choose an image a default will be added, the current image uplaoded to this page will be should below.</p>
            <br><br>
            <?php
            if ( function_exists( 'falkon_get_global_options' ) ) $falkon_option = falkon_get_global_options(); //var_dump($falkon_option);
            $page_colour = '';
            $page_ancestors = get_post_ancestors( $page->ID );
            $image_id_val = '';
            $img_src = '<img src="" class="slideimg">';
            $banner_class = 'hidden';
            $tag_class = ' hidden';
            if($mb->get_the_value()!=null){
                $image_url = wp_get_attachment_image_src( $mb->get_the_value(), 'banner-image-full' );
                $img_src = '<img src="'.$image_url[0].'" class="slideimg">';
                $image_id_val = $mb->get_the_value();
                $banner_class = '';
                ?>

                <?php
            }

            if($mb->get_the_value('banner_title')!='' or $mb->get_the_value('banner_tag')!='') $tag_class = '';

            echo '<input type="hidden" id="image_id" name="'.$mb->get_the_name().'" value="'.$mb->get_the_value().'"/>';
            ?>
            <div id="banner-preview" data-max_file_uploads="<?php echo $max_image_num;?>" class="<?php echo $banner_class.$page_colour;?>">
                <?php echo $img_src;?>
                <?php //if($mb->get_the_value('title_line')!='' or $mb->get_the_value('tag_line')!=''){ ?>

                <?php //} ?>
            </div>

        </td>
    </tr>
    </tbody>
</table>


