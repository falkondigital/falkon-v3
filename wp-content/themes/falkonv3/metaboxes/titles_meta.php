<div class="my_meta_control">
    <?php $mb->the_field('show_custom_title'); ?>
    <label for="show_custom_title"><input type="checkbox" id="show_custom_title" name="<?php $mb->the_name(); ?>" value="1"<?php $mb->the_checkbox_state('1'); ?>/> Show Custom Title?</label>
    <p class="description">If you don't want the default page title to be dsiplayed on this page, tick the box above and enter in a custom page title below.</p>
    <!--END tickbox meta-->
	<p>
		<span>Custom Title</span>
		<input type="text"  name="<?php $mb->the_name('custom-title'); ?>" value="<?php $mb->the_value('custom-title'); ?>"/>
	</p>
</div>