<?php

if ( is_admin()) add_action( 'admin_enqueue_scripts', 'my_metabox_styles_page' );
function my_metabox_styles_page()
{
//    if ( 'page' == $_POST['post_type'] && !current_user_can( 'ams_edit_pages', $post_id )) {
//        return $post_id;
//    }
//    elseif( 'page' == get_post_type( $post_id)){
//        wp_enqueue_style( 'wpalchemy-metabox-page', get_stylesheet_directory_uri() . '/metaboxes/meta-page.css');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
        wp_enqueue_script('media-upload');
        wp_enqueue_media();
        wp_enqueue_script( 'jquery-page-upload', get_template_directory_uri() .'/js/page-upload.js', array('jquery','media-upload','thickbox') );
        wp_localize_script( 'jquery-page-upload', 'jquery_page_upload', array( 'thumbnail_url' => get_template_directory_uri().'/images/default-page.jpg' ) );

        wp_enqueue_script('jquery-frontpage-upload', get_template_directory_uri() . '/js/frontpage-upload.js', array('jquery', 'media-upload', 'thickbox'));
        wp_localize_script('jquery-frontpage-upload', 'jquery_frontpage_upload', array('thumbnail_url' => get_template_directory_uri() . '/images/default-frontpage.jpg'));
//    }
}

$single_banner_mb = new WPAlchemy_MetaBox(array
(
	'id' => '_single_banner_meta',
	'title' => 'Banner control',
	'types' => array('page','project', 'post'), // added only for pages and to custom post type "events"
//	'exclude_post_id' => 5, // (Home Page)
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
	'template' => get_stylesheet_directory() . '/metaboxes/norot_banner-meta.php',
	'mode' => WPALCHEMY_MODE_EXTRACT,
	'prefix' => '_norot_banner_meta_'
));