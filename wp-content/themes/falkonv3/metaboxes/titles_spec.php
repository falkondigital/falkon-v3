<?php

$custom_titles = new WPAlchemy_MetaBox(array
(
	'id' => '_custom_titles',
	'title' => 'Custom Page Title',
	'exclude_post_id' => '6061',
	'types' => array('page'), // added only for pages and to custom post type "events"
	'context' => 'normal', // same as above, defaults to "normal"
	'priority' => 'high', // same as above, defaults to "high"
//	'save_action'	=>	'save_general_meta_data',
	'template' => get_stylesheet_directory() . '/metaboxes/titles_meta.php'
));

/* eof */